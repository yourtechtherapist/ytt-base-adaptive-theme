<?php get_header(); ?>
	
    <div id="container" class="blog index tag">
        
        <?php 
        the_archive_title( '<h1 class="page-title">', '</h1>' );
        the_archive_description( '<div class="taxonomy-description">', '</div>' ); 
        ?>

        <section class="wrapper content-width">

            <?php

            $layout = get_theme_mod('blog_layout', 'row');

            ?>

            <?php

            if ( is_active_sidebar( 'blog_widget_area' ) ) {
                $sidebar = 'sidebar_active';
            } else {
                $sidebar = 'sidebar_inactive';
            }

            ?>
				
            <div id="content" class="<?php echo $layout; ?> <?php echo $sidebar; ?>">
                
                <!-- Starts the loop -->
                <?php if ( have_posts() ) : while ( have_posts() ) : the_post() ?>
                
                <?php get_template_part( 'template-parts/blog/post' ); ?>

                <!-- Ends the loop -->
                <?php endwhile;
                wp_reset_postdata();

                else : ?>

                    <h4 class="noposts">There are no posts. 
                        <br />Please check back soon!</h4>

                <?php endif; ?>

            </div><!-- #posts -->
                
            <?php if ( is_active_sidebar( 'blog_widget_area' ) ) :
            
                $sidebar = 'sidebar_active';
                    
                ?>
            
                <aside id="sidebar">
                    <?php dynamic_sidebar( 'blog_widget_area' ); ?>
                </aside>
            
            <?php endif; ?>
			
        </section><!-- .wrapper -->

        <?php if (show_posts_nav()) : ?>

            <div class="navigation content-width">
                <div class="older button-arrow left"><?php next_posts_link('Older Posts') ?></div>
                <div class="newer"><?php previous_posts_link('Newer Posts') ?></div>
            </div>

        <?php endif; ?>

	</div><!-- #container -->

<?php get_footer(); ?>