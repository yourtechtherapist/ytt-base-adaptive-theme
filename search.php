<?php get_header(); ?>

    <div id="container" class="search archive index">

        <h1 class="page-title"><?php _e( 'Search Results for: ', 'yourtechtherapist' ); ?><span class="is-style-alt"><?php the_search_query(); ?></span></h1>

        <?php if (show_posts_nav()) : ?>

            <div class="navigation content-width">
                <div class="older button-arrow left"><?php next_posts_link('Older Results') ?></div>
                <div class="newer"><?php previous_posts_link('Newer Results') ?></div>
            </div>

        <?php endif; ?>

        <section class="wrapper content-width">
            
            <?php
            
            $layout = get_theme_mod('search_layout', 'row');
            
            ?>
            
            <?php
            
            if ( is_active_sidebar( 'blog_widget_area' ) ) {
                $sidebar = 'sidebar_active';
            } else {
                $sidebar = 'sidebar_inactive';
            }
            
            ?>
				
            <div id="content" class="<?php echo $layout; ?> <?php echo $sidebar; ?>">
                
                <?php
                
                $postsPerPage = get_theme_mod('search_posts_per_page');
                
                ?>

                <?php while ( have_posts() ) : the_post() ?>
                
                <?php get_template_part( 'template-parts/blog/post-search' ); ?>

                <!-- Ends the loop -->
                <?php endwhile; ?>

            </div><!-- #content -->
                
            <?php if ( is_active_sidebar( 'blog_widget_area' ) ) : ?>
            
                <aside id="sidebar">
                    <?php dynamic_sidebar( 'blog_widget_area' ); ?>
                </aside>
            
            <?php endif; ?>

        </section><!-- .wrapper -->

        <?php if (show_posts_nav()) : ?>

            <div class="navigation content-width">
                <div class="older"><?php next_posts_link('Older Results') ?></div>
                <div class="newer"><?php previous_posts_link('Newer Results') ?></div>
            </div>

        <?php endif; ?>
			
	</div><!-- #container -->

<?php get_footer(); ?>