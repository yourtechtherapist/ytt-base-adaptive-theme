<?php if ( true == get_theme_mod( 'header_fullscreen_search', false ) ) {
    $fullscreensearch = 'fullscreen';
}
else {
    $fullscreensearch = 'not_fullscreen';
} ?>

<div class="searchform <?php echo $fullscreensearch; ?>">
    
    <?php if ( true == get_theme_mod( 'header_fullscreen_search', false ) ) { ?>
        <span class="close"><i class="fas fa-times"></i></span>
    <?php } ?>
    
    <form method="get" id="searchform" action="<?php echo home_url() ; ?>/">
        <input type="text" placeholder="<?php esc_attr_e( 'Search', 'yourtechtherapist' ); ?>" name="s" id="searchfield" maxlength="60" />
        <button type="submit" id="searchsubmit" class="button" value="Search"><i class="fas fa-arrow-right"></i></button>
    </form>
    
</div>