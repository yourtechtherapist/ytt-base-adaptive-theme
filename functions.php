<?php

/***************************
//// 

THEME FUNCTIONS FILE

This is where all WordPress theme functions live. Make sure that you remove anything you do not need here so that there is no extra bloat.

////
***************************/


/*****************
//// INCLUDES ////
*****************/

/* Including external functions files to organize code. These files can be found in the functions folder in the root of the theme. */
$roots_includes = array(
    
    /*******************
    //// CUSTOMIZER ////
    *******************/
    '/functions/customizer/kirkiconfig.php',
    '/functions/customizer/customfonts.php',
    '/functions/customizer/customfontsscript.php',
    '/functions/customizer/header.php',
    '/functions/customizer/footer.php',
    '/functions/customizer/content.php',
    '/functions/customizer/layout.php',
    '/functions/customizer/fonts.php',
    '/functions/customizer/colors.php',
    '/functions/customizer/identity.php',
    '/functions/customizer/mobile.php',
    '/functions/customizer/generatecss.php',
    '/functions/customizer/announcementbar.php',
    '/functions/customizer/additionalcode.php',
    '/functions/customizer/blog.php',
    
    /*****************
    //// SETTINGS ////
    *****************/
    '/functions/settings/pages.php',
    
    /******************
    //// GUTENBERG ////
    ******************/
    '/functions/gutenberg/blocks.php',
    '/functions/gutenberg/colors.php',

    /*********************
    //// WIDGET AREAS ////
    *********************/
    '/functions/widgets/widgetareas.php',
    '/functions/widgets/widgets.php',
    
    /*******************
    //// SHORTCODES ////
    *******************/
    '/functions/shortcodes.php',
    
    /*************
    //// BLOG ////
    *************/
    '/functions/blog/index.php',
    '/functions/blog/comments.php',
    
    /**************
    //// TGMPA ////
    **************/
    '/functions/tgmpa.php'
    
);

foreach($roots_includes as $file){
    if(!$filepath = locate_template($file)) {
        trigger_error("Error locating `$file` for inclusion!", E_USER_ERROR);
    }

        require_once $filepath;
    }
unset($file, $filepath);


/********************************
//// CUSTOMIZER SANITIZATION ////
********************************/

// Sanitize checkboxes
function checkbox_sanitization( $input ) {
   if ( true === $input ) {
      return 1;
   } else {
      return 0;
   }
}

// Sanitize radio buttons
function radio_sanitization( $input, $setting ){

    //input must be a slug: lowercase alphanumeric characters, dashes and underscores are allowed only
    $input = sanitize_key($input);

    //get the list of possible radio box options 
    $choices = $setting->manager->get_control( $setting->id )->choices;

    //return input if valid or return default option
    return ( array_key_exists( $input, $choices ) ? $input : $setting->default );                

}


/*********************
//// LOCALIZATION ////
*********************/

// Make theme available for translation
// Translations can be filed in the /languages/ directory
load_theme_textdomain( 'yourtechtherapist', TEMPLATEPATH . '/languages' );

$locale = get_locale();
$locale_file = TEMPLATEPATH . "/languages/$locale.php";
if ( is_readable($locale_file) )
    require_once($locale_file);


/************************
//// BASIC FUNCTIONS ////
************************/

// Theme support

// Set content width value based on the theme's design
if ( ! isset( $content_width ) )
	$content_width = 1400;

// Register Theme Features
function theme_setup()  {

	// Add theme support for Automatic Feed Links
	add_theme_support( 'automatic-feed-links' );

	// Add theme support for Featured Images
	add_theme_support( 'post-thumbnails' );

	// Add theme support for Custom Background
	$background_args = array(
		'default-color'          => 'ffffff',
		'default-repeat'         => 'no-repeat',
		'default-position-x'     => 'center',
	);
	add_theme_support( 'custom-background', $background_args );

	// Add theme support for HTML5 Semantic Markup
	add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list', 'gallery', 'caption' ) );

	// Add theme support for document Title tag
	add_theme_support( 'title-tag' );
    
    // Add theme suport for Align Wide
    add_theme_support( 'align-wide' );
    
    // Add theme support for custom logos
    add_theme_support( 'custom-logo' );
    
    // Add theme support for responsive embeds
    add_theme_support( 'responsive-embeds' );
}
add_action( 'after_setup_theme', 'theme_setup' );

// Enable shortcodes in text fields
add_filter( 'widget_text', 'do_shortcode' );

// Editor Styles
add_theme_support( 'editor-styles' );
add_editor_style( 'style-editor.css' );
function post_editor_styles() {
    global $post;
    $post_type = get_post_type( $post->ID );
    $editor_style = 'style-editor-' . $post_type . '.css';
    add_editor_style( $editor_style );
}
add_editor_style( 'https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Roboto:300,300i,400,400i,500,500i,700,700i&display=swap' );

// Home Page in Custom Menus
function home_page_menu_args( $args ) {
$args['show_home'] = true;
return $args;
}
add_filter( 'wp_page_menu_args', 'home_page_menu_args' );

// Get the page number
function get_page_number() {
    if ( get_query_var('paged') ) {
        print ' | ' . __( 'Page ' , 'yourtechtherapist') . get_query_var('paged');
    }
} // end get_page_number

// RSS Featured Image Size
function thumbnail_size_for_rss( $default ) {
return 'medium';
}
add_filter( 'rfi_rss_image_size', 'thumbnail_size_for_rss', 10, 1 );

// Custom Image Sizes
if ( function_exists( 'add_theme_support' ) ) {
    add_image_size( 'wide_featured', 1920, 9999 );
    add_image_size( 'index_featured', 1200, 9999 );
 }


/************************
//// INCLUDE ACF PRO ////
************************/

// Save ACF JSON Fields
add_filter('acf/settings/save_json', 'my_acf_json_save_point');
 
function my_acf_json_save_point( $path ) {
    
    // update path
    $path = get_stylesheet_directory() . '/includes/acf/fields';
    
    
    // return
    return $path;
    
}

// Load ACF JSON Fields
add_filter('acf/settings/load_json', 'my_acf_json_load_point');

function my_acf_json_load_point( $paths ) {
    
    // remove original path (optional)
    unset($paths[0]);
    
    
    // append path
    $paths[] = get_stylesheet_directory() . '/includes/acf/fields';
    
    
    // return
    return $paths;
    
}


/***********************
//// BLOG FUNCTIONS ////
***********************/

// Post Categories
function category_id_class($classes) {
     global $post;
          foreach((get_the_category($post->ID)) as $category)
               $classes [] = 'cat-' . $category->cat_ID . '-id';
               
     return $classes;
}

// Comments
// Include Better Comments
require_once get_parent_theme_file_path( 'template-parts/blog/better-comments.php' );


// For category lists on category archives: Returns other categories except the current one (redundant)
function cats_meow($glue) {
    $current_cat = single_cat_title( '', false );
    $separator = "\n";
    $cats = explode( $separator, get_the_category_list($separator) );
    foreach ( $cats as $i => $str ) {
        if ( strstr( $str, ">$current_cat<" ) ) {
            unset($cats[$i]);
            break;
        }
    }
    if ( empty($cats) )
        return false;
 
    return trim(join( $glue, $cats ));
} // end cats_meow


// For tag lists on tag archives: Returns other tags except the current one (redundant)
function tag_ur_it($glue) {
    $current_tag = single_tag_title( '', '',  false );
    $separator = "\n";
    $tags = explode( $separator, get_the_tag_list( "", "$separator", "" ) );
    foreach ( $tags as $i => $str ) {
        if ( strstr( $str, ">$current_tag<" ) ) {
            unset($tags[$i]);
            break;
        }
    }
    if ( empty($tags) )
        return false;
 
    return trim(join( $glue, $tags ));
} // end tag_ur_it

// Detect post view account for Most Popular Posts
function wpb_set_post_views($postID) {
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}
//To keep the count accurate, lets get rid of prefetching
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);

// Avatar Classes
add_filter('get_avatar','add_gravatar_class');

function add_gravatar_class($class) {
    $class = str_replace("class='avatar", "class='author avatar", $class);
    return $class;
}


/***********************
//// MENU LOCATIONS ////
***********************/

// Menu's
function register_my_menus() {
  register_nav_menus(
    array(
      'header-menu' => __( 'Header Menu', 'yourtechtherapist' ),
      'footer-menu' => __( 'Footer Menu', 'yourtechtherapist' ),
      'secondary-header-menu' => __( 'Secondary Header Menu', 'yourtechtherapist' ),
      'social-menu' => __( 'Social Menu', 'yourtechtherapist' )
    )
  );
}
add_action( 'init', 'register_my_menus' );


/*******************************
//// DYNAMIC COPYRIGHT TEXT ////
*******************************/

// copyright
function copyright() {
global $wpdb;
$copyright_dates = $wpdb->get_results("
SELECT
YEAR(min(post_date_gmt)) AS firstdate,
YEAR(max(post_date_gmt)) AS lastdate
FROM
$wpdb->posts
WHERE
post_status = 'publish'
");
$output = '';
if($copyright_dates) {
$copyright = "&copy; " . $copyright_dates[0]->firstdate;
if($copyright_dates[0]->firstdate != $copyright_dates[0]->lastdate) {
$copyright .= '-' . $copyright_dates[0]->lastdate;
}
$output = $copyright;
}
return $output;
}


/***********************************
//// ENQUEUE SCRIPTS AND STYLES ////
***********************************/

// Scripts (enqueue)
function scripts_and_styles()  
{   
    // Register the script like this for a theme:  
    wp_register_script( 'main', get_template_directory_uri() . '/js/main.js', array( 'jquery','jquery-ui-core','jquery-ui-position' ) );
    wp_register_script( 'nav', get_template_directory_uri() . '/js/nav.js', array( 'jquery','jquery-ui-core', 'jquery-effects-slide') );
    wp_register_script( 'header', get_template_directory_uri() . '/js/header.js', array( 'jquery' ) );
    wp_register_script( 'modernizr', get_template_directory_uri() . '/js/modernizr.js', array( 'jquery' ) );
    wp_register_script( 'respond', get_template_directory_uri() . '/js/respond.min.js', array( 'jquery' ) );
    wp_register_script( 'honerintent', get_template_directory_uri() . '/js/jquery.hoverIntent.min.js', array( 'jquery' ) );
    wp_register_script( 'select', get_template_directory_uri() . '/js/select2.min.js' );
  
    // For either a plugin or a theme, you can then enqueue the script:  
    wp_enqueue_script( 'main' );
    wp_enqueue_script( 'nav' );
    wp_enqueue_script( 'header' );
    wp_enqueue_script( 'modernizr' );
    wp_enqueue_script( 'respond' );
    wp_enqueue_script( 'honerintent' );
    wp_enqueue_script( 'select' );
    wp_enqueue_script( 'jquery-ui-position' );
    wp_enqueue_script( 'jquery-effects-slide' );
    
    // Enqueue stylesheets
    wp_enqueue_style( 'style', get_stylesheet_uri() );
    wp_enqueue_style( 'select', get_template_directory_uri() . '/css/select2.min.css' );
    
}  
add_action( 'wp_enqueue_scripts', 'scripts_and_styles' ); 

// Customizer Scripts
function customizer_scripts()  
{   
    
    
    if ( true == get_theme_mod('button_hover_swap', true) ) {
    
        // Register the script like this for a theme:  
        wp_register_script( 'buttonswap', get_template_directory_uri() . '/js/buttonswap.js', array( 'jquery' ) );

        // For either a plugin or a theme, you can then enqueue the script:  
        wp_enqueue_script( 'buttonswap' );
        
    } else {
        // Don't enqueue the script
    }
    
}  
add_action( 'enqueue_block_assets', 'customizer_scripts' ); 


function be_gutenberg_scripts() {

	wp_enqueue_script(
		'blockstyles', 
		get_stylesheet_directory_uri() . '/js/blockstyles.js', 
		array( 'wp-blocks', 'wp-dom' ), 
		filemtime( get_stylesheet_directory() . '/js/blockstyles.js' ),
		true
	);
}
add_action( 'enqueue_block_editor_assets', 'be_gutenberg_scripts' );


add_filter('post_class', 'category_id_class');
add_filter('body_class', 'category_id_class');


/*****************
//// EXCERPTS ////
*****************/

// Replaces the excerpt "more" text by a link
function new_excerpt_more($more) {
    global $post;
    if ($post->post_type == 'post') {
        return '... <a class="moretag" href="'. get_permalink($post->ID) . '"> Read More</a>';
    }
}
add_filter('excerpt_more', 'new_excerpt_more');


/****************************
//// WEBSITE SCREENSHOTS ////
****************************/

//Automatic URL Screenshots for resources page
add_shortcode('ss_screenshot', 'ss_screenshot_shortcode');
 
function ss_screenshot_shortcode($atts){
 
  $width = intval($atts['width']);
   
  $width = (100 <= $width && $width <= 1200) ? $width : 1000;
   
  $site = trim($atts['site']);
   
  if ($site != ''){
   
    $query_url =  'https://s.wordpress.com/mshots/v1/' . urlencode($site) . '?w=' . $width;
     
    $image_tag = '<img class="ss_screenshot_img" alt="' . $site . '" width="' . $width . '" src="' . $query_url . '" />';
     
    echo $image_tag; 
   
  }else{
   
    echo 'Bad screenshot url!';
   
  }
}


/**************************
//// CUSTOM POST TYPES ////
**************************/

function custom_post_type() {
 
// Set UI labels for Custom Post Type
    $labels = array(
        'name'                => _x( 'Portfolio', 'Post Type General Name', 'twentythirteen' ),
        'singular_name'       => _x( 'Portfolio Item', 'Post Type Singular Name', 'twentythirteen' ),
        'menu_name'           => __( 'Portfolio', 'twentythirteen' ),
        'all_items'           => __( 'All Portfolio Items', 'twentythirteen' ),
        'add_new_item'        => __( 'Add New Portfolio Item', 'twentythirteen' ),
        'add_new'             => __( 'Add New', 'twentythirteen' ),
        'not_found'           => __( 'Not Found', 'twentythirteen' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'twentythirteen' ),
    );
     
// Set other options for Custom Post Type
     
    $args = array(
        'label'               => __( 'portfolio', 'twentythirteen' ),
        'description'         => __( 'Your Tech Therapist design portfolio', 'twentythirteen' ),
        'labels'              => $labels,
        // Features this CPT supports in Post Editor
        'supports'            => array( 'title', 'excerpt', 'editor', 'thumbnail', 'custom-fields', ),
        // You can associate this CPT with a taxonomy or custom taxonomy. 
        'taxonomies'          => array( 'portfolio' ),
        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.
        */ 
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'page',
    );
     
    // Registering your Custom Post Type
    register_post_type( 'portfolio', $args );
 
}
 
/* Hook into the 'init' action so that the function
* Containing our post type registration is not 
* unnecessarily executed. 
*/
 
add_action( 'init', 'custom_post_type', 0 );



/***************************
//// CUSTOM ADMIN MENUS ////
***************************/

// Creates settings page for YTT Plugins
function ytt_plugins_page()
{
    add_menu_page( 'Your Tech Therapist Plugins', 'YTT Plugins', 'manage_options', 'ytt-plugins-admin-page.php', 'ytt_plugins_page_content' );
}

add_action("admin_menu", "ytt_plugins_page");


// YTT Plugins page content
function ytt_plugins_page_content() {
    ?>
    <div class="wrap">
        <h1>Your Tech Therapist Plugins</h1>

        <div class="content">
            <p>If you have a YTT Plugin installed that has settings it will create a submenu here</p>
        </div>
    </div>
    <?php
}



