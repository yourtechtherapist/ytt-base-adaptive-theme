## YTT Adaptive Theme

This is the Your Tech Therapist Adaptive theme. This can be used as a starting point for all YTT web projects. It is developed to be a one size fits all platform for quick WordPress websites.

This theme uses the Kirk Customizer Framework to enable customization through the WordPress Customizer, and requires Kirki to function. It also requires Advanced Custom Fields Pro.

This theme can also be used as a jumping off point for custom development if the built in options are not enough to meet the projects needs.

Upon installation, the theme will require you to install certain plugins in order for it to work. It will also recommend additional plugins that may be useful for added functionality such as Contact Form 7, YTT Outdated Browser, and others.

---

##  Requirements

This theme requires two additional plugins to function properly.

1. Kirki Customizer Framework
2. Advanced Custom Fiels Pro

**It is also recommended that you install these plugins**

1. YTT Outdated Browser
2. Advanced Custom Fields: Theme Code Pro
3. Contact Form 7

---

##  Installation

Download the **yttadaptivetheme.zip** either upload the .zip file under **Appearance > Themes**, or you can upload this theme to your WordPress development environment directly under **wp-content/themes**.

Download the ACF settings export, install Advanced Custom Fields Pro, and import the ACF settings through the ACF import function to properly set up the ACF fields within the Theme Settings menu.