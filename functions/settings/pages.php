<?php

/***************************
//// 

SETTINGS - PAGES

Settings options on a per page basis, changed via the sidebar in the WordPress editor.

////
***************************/


/**********************
//// PAGE SETTINGS ////
**********************/

// ACF Filters

/*
add_filter('acf/load_field/name=transparent_header', function($field) {
    global $post;
    
    // variant 1 (use get_post_meta instead of get_field to avoid conflicts)
    if ( true == get_theme_mod( 'transparent_header', true ) ) {
        $field['default_value'] = 1;
    } else {
        $field['default_value'] = 0;
    }
    
    return $field;          
});
*/