<?php

/***************************
//// 

CUSTOMIZER - HEADER

Header Settings in the WordPress Customizer.

////
***************************/


/************************
//// HEADER CONTROLS ////
************************/

// Kirki

// Header Panel
Kirki::add_panel( 'header_panel', array(
    'priority'    => 160,
    'title'       => esc_html__( 'Header', 'yourtechtherapist' ),
    'description' => esc_html__( 'Adjust your Header settings.', 'yourtechtherapist' ),
) );


// Header Color Section
Kirki::add_section( 'header_color', array(
    'title'          => esc_html__( 'Header Color', 'yourtechtherapist' ),
    'description'    => esc_html__( 'Adjust header colors.', 'yourtechtherapist' ),
    'panel'          => 'header_panel',
    'priority'       => 70,
) );

// Transparent Header
Kirki::add_field( 'ytt', [
	'type'        => 'toggle',
	'settings'    => 'transparent_header',
	'label'       => esc_html__( 'Transparent Header', 'yourtechtherapist' ),
	'description' => esc_html__( 'Make the header transparent and allow the page content to show behind it. This allows you to use blocks in Gutenberg to change header background. THIS SETTING WILL OVERRIDE THE HEADER BACKGROUND COLOR.', 'yourtechtherapist' ),
	'section'     => 'header_color',
	'default'     => 0,
    'active_callback'  => [
        [
            'setting'  => 'header_location',
            'operator' => '!==',
            'value'    => 'location_left',
        ],
        [
            'setting'  => 'header_location',
            'operator' => '!==',
            'value'    => 'location_right',
        ],
    ],
] );

// Header Background Color
Kirki::add_field( 'ytt', [
	'type'        => 'color',
	'settings'    => 'header_background',
	'label'       => __( 'Header Background Color', 'yourtechtherapist' ),
	'description' => esc_html__( 'Change the header background color.', 'yourtechtherapist' ),
	'section'     => 'header_color',
	'default'     => '#349BB3',
    'output'      => [
        'element' => 'header.primary, header.primary nav ul li ul, header.primary nav .searchform',
        'property' => 'background-color',
    ],
] );

// Header Border
Kirki::add_field( 'ytt', [
	'type'        => 'toggle',
	'settings'    => 'header_border',
	'label'       => esc_html__( 'Header Border', 'yourtechtherapist' ),
	'section'     => 'header_color',
	'default'     => 0,
] );

// Header Border Color
Kirki::add_field( 'ytt', [
	'type'        => 'color',
	'settings'    => 'header_border_color',
	'label'       => __( 'Header Border Color', 'yourtechtherapist' ),
	'description' => esc_html__( 'Change the header border color.', 'yourtechtherapist' ),
	'section'     => 'header_color',
	'default'     => '#349BB3',
    'active_callback' => [
        [
            'setting'  => 'header_border',
            'operator' => '==',
            'value'    => true,
        ]
    ],
    'output'      => [
        'element' => 'header.primary',
        'property' => 'border-color',
    ],
] );

// Header Border Width
Kirki::add_field( 'ytt', [
	'type'        => 'dimensions',
	'settings'    => 'header_border-width',
	'label'       => esc_html__( 'Header Border Width', 'yourtechtherapist' ),
	'section'     => 'header_color',
    'active_callback' => [
        [
            'setting'  => 'header_border',
            'operator' => '==',
            'value'    => true,
        ]
    ],
	'default'     => [
		'border-top-width'  => '0.1rem',
		'border-bottom-width'  => '0.1rem',
        'border-left-width'  => '0.1rem',
        'border-right-width'  => '0.1rem',
	],
    'choices'     => [
		'labels'  => [
			'border-top-width'  => esc_html__( 'Top', 'yourtechtherapist' ),
			'border-bottom-width'  => esc_html__( 'Bottom', 'yourtechtherapist' ),
			'border-left-width' => esc_html__( 'Left', 'yourtechtherapist' ),
			'border-right-width' => esc_html__( 'Right', 'yourtechtherapist' ),
		],
	],
    'output'      => [
        'element'  => '.header_location header.primary',
    ],
] );

// Main Header Font
Kirki::add_field( 'ytt', [
    'type'        => 'typography',
    'settings'    => 'main_header_font',
    'label'       => esc_html__( 'Main Header Font', 'yourtechtherapist' ),
    'section'     => 'header_color',
    'theme_config' => 'yourtechtherapist',
    'default'     => [
        'font-family'    => 'Open Sans',
        'variant'        => 'regular',
        'font-size'      => '1em',
        'line-height'    => '1.5',
        'letter-spacing' => '0',
        'color'          => '#333333',
        'text-transform' => 'none',
        'text-align'     => 'center',
    ],
    'priority'    => 10,
    'transport'   => 'auto',
    'choices' => leedo_add_custom_choice(),
    'output'      => [ 
        'element' => 'header.primary > .wrapper',
    ],
] );

// Main Header Link Color
Kirki::add_field( 'ytt', [
    'type'        => 'multicolor',
    'settings'    => 'header_link_colors',
    'label'       => esc_html__( 'Header Link Colors', 'kirki' ),
    'section'     => 'header_color',
    'priority'    => 10,
    'choices'     => [
        'default'    => esc_html__( 'Normal', 'kirki' ),
        'hover'   => esc_html__( 'Hover', 'kirki' ),
        'visited'  => esc_html__( 'Visited', 'kirki' ),
    ],
    'default'     => [
        'default'    => '#349BB3',
        'hover'   => '#74E6FF',
        'visited'  => '#74E6FF',
    ],
    'output'      => [
        [
            'choice' => 'default',
            'element' => 'header#primaryheader > .wrapper a',
            'property' => 'color',
        ],
        [
            'choice' => 'visited',
            'element' => 'header#primaryheader > .wrapper a:visited',
            'property' => 'color',
        ],
        [
            'choice' => 'hover',
            'element' => ['header#primaryheader > .wrapper a:hover', 'header#primaryheader > .wrapper a:hover .has-text-color'],
            'property' => 'color',
        ],
    ],
] );


// Header Layout Section
Kirki::add_section( 'header_layout', array(
    'title'          => esc_html__( 'Header Layout', 'yourtechtherapist' ),
    'description'    => esc_html__( 'Adjust header layout.', 'yourtechtherapist' ),
    'panel'          => 'header_panel',
    'priority'       => 70,
) );

// Header Location
Kirki::add_field( 'ytt', [
	'type'        => 'select',
	'settings'    => 'header_location',
	'label'       => esc_html__( 'Header Location', 'yourtechtherapist' ),
    'description' => esc_html__( 'Change the header location on all pages.' ),
	'section'     => 'header_layout',
	'default'     => 'location_top',
	'placeholder' => esc_html__( 'Select an option...', 'yourtechtherapist' ),
	'priority'    => 10,
	'multiple'    => 1,
	'choices'     => [
		'location_top' => esc_html__( 'Top', 'yourtechtherapist' ),
		'bottom' => esc_html__( 'Bottom', 'yourtechtherapist' ),
		'location_left' => esc_html__( 'Left', 'yourtechtherapist' ),
		'location_right' => esc_html__( 'Right', 'yourtechtherapist' ),
	],
] );

// Sticky Header
Kirki::add_field( 'ytt', [
	'type'        => 'toggle',
	'settings'    => 'sticky_header',
	'label'       => esc_html__( 'Sticky Header', 'yourtechtherapist' ),
	'description' => esc_html__( 'Make the header sticky. It will scroll with the page.', 'yourtechtherapist' ),
	'section'     => 'header_layout',
	'default'     => 0,
] );

// Header Padding
Kirki::add_field( 'ytt', [
	'type'        => 'dimensions',
	'settings'    => 'header_padding',
	'label'       => esc_html__( 'Header Padding', 'yourtechtherapist' ),
	'description' => esc_html__( 'Add padding to the header.', 'yourtechtherapist' ),
	'section'     => 'header_layout',
	'default'     => [
		'padding-top'  => '1rem',
		'padding-bottom'  => '1rem',
        'padding-left'  => '1rem',
        'padding-right'  => '1rem',
	],
    'choices'     => [
		'labels'  => [
			'padding-top'  => esc_html__( 'Top', 'yourtechtherapist' ),
			'padding-bottom'  => esc_html__( 'Bottom', 'yourtechtherapist' ),
			'padding-left' => esc_html__( 'Left', 'yourtechtherapist' ),
			'padding-right' => esc_html__( 'Right', 'yourtechtherapist' ),
		],
	],
    'output'      => [
        [
            'element'  => 'header.primary > .wrapper',
        ],
        [
            'element'  => 'header.primary.sticky_header .wrapper',
            'value_pattern' => 'calc($ / 2)'
        ],
    ],
] );

// Header Container Width
Kirki::add_field( 'ytt', [
	'type'        => 'dimension',
	'settings'    => 'header_width',
	'label'       => esc_html__( 'Header Container Width', 'yourtechtherapist' ),
    'description'    => esc_html__( 'Adjust the width of the header container.', 'yourtechtherapist' ),
	'section'     => 'header_layout',
	'default'     => '100%',
	'priority'    => 10,
    'output'      => [
        'element' => 'header.primary',
        'property' => 'max-width',
    ],
] );

// Header Content Width
Kirki::add_field( 'ytt', [
	'type'        => 'dimension',
	'settings'    => 'header_content_width',
	'label'       => esc_html__( 'Header Content Width', 'yourtechtherapist' ),
    'description'    => esc_html__( 'Adjust the width of the header content.', 'yourtechtherapist' ),
	'section'     => 'header_layout',
	'default'     => '100%',
	'priority'    => 10,
    'output'      => [
        'element' => 'header.primary .wrapper',
        'property' => 'max-width',
    ],
] );

// Header Content layout
Kirki::add_field( 'ytt', [
	'type'        => 'select',
	'settings'    => 'header_content_layout',
	'label'       => esc_html__( 'Content Layout', 'yourtechtherapist' ),
    'description' => esc_html__( 'Change the Adjust the layout of the header.' ),
	'section'     => 'header_layout',
	'default'     => 'layout_horizontal',
	'placeholder' => esc_html__( 'Select an option...', 'yourtechtherapist' ),
	'priority'    => 10,
	'multiple'    => 1,
	'choices'     => [
		'layout_horizontal' => esc_html__( 'Horizontal Layout', 'yourtechtherapist' ),
		'layout_stacked' => esc_html__( 'Stacked Layout', 'yourtechtherapist' ),
		'layout_reversed' => esc_html__( 'Reversed Horizontal', 'yourtechtherapist' ),
	],
] );

// Show Navigation
Kirki::add_field( 'ytt', [
	'type'        => 'toggle',
	'settings'    => 'header_show_navigation',
	'label'       => esc_html__( 'Show Navigation Menu', 'yourtechtherapist' ),
	'description' => esc_html__( 'Show the navigation menu in the header.', 'yourtechtherapist' ),
	'section'     => 'header_layout',
	'default'     => 1,
] );

// Show Search
Kirki::add_field( 'ytt', [
	'type'        => 'toggle',
	'settings'    => 'header_show_search',
	'label'       => esc_html__( 'Show Search Icon', 'yourtechtherapist' ),
	'description' => esc_html__( 'Show the search icon in the header.', 'yourtechtherapist' ),
	'section'     => 'header_layout',
	'default'     => 1,
] );

// Fullscreen Search
Kirki::add_field( 'ytt', [
	'type'        => 'toggle',
	'settings'    => 'header_fullscreen_search',
	'label'       => esc_html__( 'Fullscreen Search Field', 'yourtechtherapist' ),
	'description' => esc_html__( 'Show a fullscreen search field when the search icon is clicked in the header. (Color options can be found in Header > Header Color)', 'yourtechtherapist' ),
	'section'     => 'header_layout',
    'active_callback' => [
        [
            'setting'  => 'header_show_search',
            'operator' => '==',
            'value'    => true,
        ]
    ],
	'default'     => 0,
] );

// Fullscreen Search Background
Kirki::add_field( 'ytt', [
	'type'        => 'color',
	'settings'    => 'header_fullscreen_search_background',
	'label'       => esc_html__( 'Fullscreen Search Field Background', 'yourtechtherapist' ),
	'section'     => 'header_color',
    'active_callback' => [
        [
            'setting'  => 'header_fullscreen_search',
            'operator' => '==',
            'value'    => true,
        ]
    ],
	'default'     => 'rgb(51, 51, 51, 0.95)',
	'choices'     => [
		'alpha' => true,
	],
    'output'      => [
        'element' => 'header.primary nav .searchform.fullscreen',
        'property' => 'background-color',
    ],
] );

// Fullscreen Search Text
Kirki::add_field( 'ytt', [
	'type'        => 'color',
	'settings'    => 'header_fullscreen_search_text',
	'label'       => esc_html__( 'Fullscreen Search Field Text', 'yourtechtherapist' ),
	'section'     => 'header_color',
    'active_callback' => [
        [
            'setting'  => 'header_fullscreen_search',
            'operator' => '==',
            'value'    => true,
        ]
    ],
	'default'     => '#ffffff',
    'output'      => [
        'element' => 'header .searchform.fullscreen input',
        'property' => 'color',
    ],
] );

// Fullscreen Search Placeholder Color
Kirki::add_field( 'ytt', [
	'type'        => 'color',
	'settings'    => 'header_fullscreen_search_placeholder',
	'label'       => __( 'Fullscreen Search Placeholder', 'yourtechtherapist' ),
	'description' => esc_html__( 'Change fullscreen search form field placeholder color.', 'yourtechtherapist' ),
	'section'     => 'header_color',
	'default'     => '#333333',
    'output'      => [
        [
            'element' => 'header .searchform.fullscreen *::placeholder',
            'property' => 'color',
        ],
        [
            'element' => 'header .searchform.fullscreen *::-webkit-input-placeholder',
            'property' => 'color',
        ],
        [
            'element' => 'header .searchform.fullscreen *::-moz-placeholder',
            'property' => 'color',
        ],
        [
            'element' => 'header .searchform.fullscreen *:-ms-input-placeholder',
            'property' => 'color',
        ],
        [
            'element' => 'header .searchform.fullscreen *:moz-placeholder',
            'property' => 'color',
        ],
    ],
] );

// Header Dropdown Menu
Kirki::add_section( 'header_dropdown_menu', array(
    'title'          => esc_html__( 'Dropdown Menus', 'yourtechtherapist' ),
    'description'    => esc_html__( 'Adjust styles for the dropdown menus.', 'yourtechtherapist' ),
    'panel'          => 'header_panel',
    'priority'       => 70,
) );

// Dropdown Background
Kirki::add_field( 'ytt', [
	'type'        => 'color',
	'settings'    => 'header_dropdown_background',
	'label'       => __( 'Dropdown Background Color', 'yourtechtherapist' ),
	'description' => esc_html__( 'Change the dropdown menu background color.', 'yourtechtherapist' ),
	'section'     => 'header_dropdown_menu',
	'default'     => '#349BB3',
    'output'      => [
        'element' => ['header.primary nav#nav ul li > ul', 'header.primary .secondary nav ul li > ul'],
        'property' => 'background-color',
        'media_query' => '@media (min-width: 769px)',
    ],
	'choices'     => [
		'alpha' => true,
	],
] );

// Dropdown Color
Kirki::add_field( 'ytt', [
	'type'        => 'color',
	'settings'    => 'header_dropdown_text_color',
	'label'       => __( 'Dropdown Text Color', 'yourtechtherapist' ),
	'description' => esc_html__( 'Change the dropdown menu text color.', 'yourtechtherapist' ),
	'section'     => 'header_dropdown_menu',
	'default'     => '#FFFFFF',
    'output'      => [
        'element' => 'header.primary nav#nav ul li > ul a',
        'property' => 'color',
        'media_query' => '@media (min-width: 769px)',
    ],
] );

// Dropdown Hover Color
Kirki::add_field( 'ytt', [
	'type'        => 'color',
	'settings'    => 'header_dropdown_text_hover_color',
	'label'       => __( 'Dropdown Text Hover Color', 'yourtechtherapist' ),
	'description' => esc_html__( 'Change the dropdown menu text hover color.', 'yourtechtherapist' ),
	'section'     => 'header_dropdown_menu',
	'default'     => '#FFFFFF',
    'output'      => [
        'element' => 'header.primary nav#nav ul li > ul a:hover',
        'property' => 'color',
        'media_query' => '@media (min-width: 769px)',
    ],
] );

// Dropdown Border Color
Kirki::add_field( 'ytt', [
	'type'        => 'color',
	'settings'    => 'header_dropdown_border_color',
	'label'       => __( 'Dropdown Border Color', 'yourtechtherapist' ),
	'description' => esc_html__( 'Change the dropdown menu border color.', 'yourtechtherapist' ),
	'section'     => 'header_dropdown_menu',
	'default'     => '#FFFFFF',
    'output'      => [
        'element' => ['header.primary nav#nav ul li > ul', 'header.primary .secondary nav ul li > ul'],
        'property' => 'border-color',
        'media_query' => '@media (min-width: 769px)',
    ],
	'choices'     => [
		'alpha' => true,
	],
] );

// Secondary Header Section
Kirki::add_section( 'secondary_header', array(
    'title'          => esc_html__( 'Secondary Header', 'yourtechtherapist' ),
    'description'    => esc_html__( 'Adjust the secondary header options.', 'yourtechtherapist' ),
    'panel'          => 'header_panel',
    'priority'       => 70,
) );

// Show Secondary Header
Kirki::add_field( 'ytt', [
	'type'        => 'toggle',
	'settings'    => 'show_secondary_header',
	'label'       => esc_html__( 'Show Secondary Header', 'yourtechtherapist' ),
	'section'     => 'secondary_header',
	'default'     => 0,
] );

// Show Secondary Header Menu
Kirki::add_field( 'ytt', [
	'type'        => 'toggle',
	'settings'    => 'show_secondary_header_menu',
	'label'       => esc_html__( 'Show Secondary Header Menu', 'yourtechtherapist' ),
	'section'     => 'secondary_header',
    'active_callback' => [
        [
            'setting'  => 'show_secondary_header',
            'operator' => '==',
            'value'    => true,
        ]
    ],
	'default'     => 0,
] );

// Show Social Menu
Kirki::add_field( 'ytt', [
	'type'        => 'toggle',
	'settings'    => 'show_social_menu',
	'label'       => esc_html__( 'Show Social Menu', 'yourtechtherapist' ),
	'section'     => 'secondary_header',
    'active_callback' => [
        [
            'setting'  => 'show_secondary_header',
            'operator' => '==',
            'value'    => true,
        ]
    ],
	'default'     => 0,
] );

// Secondary Header Text Area
Kirki::add_field( 'ytt', [
    'type'        => 'textarea',
    'settings'    => 'secondary_header_text',
    'label'       => esc_html__( 'Text To Display', 'yourtechtherapist' ),
    'description' => esc_html__( 'Display text to the left of the menus(supports shortcodes).', 'yourtechtherapist' ),
    'section'     => 'secondary_header',
    'active_callback' => [
        [
            'setting'  => 'show_secondary_header',
            'operator' => '==',
            'value'    => true,
        ]
    ],
    'default'     => '',
]);

// Secondary Header Background
Kirki::add_field( 'ytt', [
	'type'        => 'color',
	'settings'    => 'secondary_header_background',
	'label'       => __( 'Secondary Header Background Color', 'yourtechtherapist' ),
	'description' => esc_html__( 'Change the background color of the secondary header.', 'yourtechtherapist' ),
	'section'     => 'secondary_header',
	'default'     => '#333333',
    'active_callback' => [
        [
            'setting'  => 'show_secondary_header',
            'operator' => '==',
            'value'    => true,
        ]
    ],
    'output'      => [
        'element' => 'header.primary > .secondary',
        'property' => 'background-color',
    ],
] );

// Secondary Header Border
Kirki::add_field( 'ytt', [
	'type'        => 'toggle',
	'settings'    => 'secondary_header_border',
	'label'       => esc_html__( 'Secondary Header Border', 'yourtechtherapist' ),
	'section'     => 'secondary_header',
    'active_callback' => [
        [
            'setting'  => 'show_secondary_header',
            'operator' => '==',
            'value'    => true,
        ]
    ],
	'default'     => 0,
] );

// Secondary Header Border Color
Kirki::add_field( 'ytt', [
	'type'        => 'color',
	'settings'    => 'secondary_header_border_color',
	'label'       => __( 'Secondary Header Border Color', 'yourtechtherapist' ),
	'description' => esc_html__( 'Change the header border color.', 'yourtechtherapist' ),
	'section'     => 'secondary_header',
	'default'     => '#349BB3',
    'active_callback' => [
        [
            'setting'  => 'secondary_header_border',
            'operator' => '==',
            'value'    => true,
        ]
    ],
    'output'      => [
        'element' => 'header.primary > .secondary',
        'property' => 'border-color',
    ],
] );

// Secondary Header Border Width
Kirki::add_field( 'ytt', [
	'type'        => 'dimensions',
	'settings'    => 'secondary_header_border-width',
	'label'       => esc_html__( 'Secondary Header Border Width', 'yourtechtherapist' ),
	'section'     => 'secondary_header',
    'active_callback' => [
        [
            'setting'  => 'secondary_header_border',
            'operator' => '==',
            'value'    => true,
        ]
    ],
	'default'     => [
		'border-top-width'  => '0.1rem',
		'border-bottom-width'  => '0.1rem',
        'border-left-width'  => '0.1rem',
        'border-right-width'  => '0.1rem',
	],
    'choices'     => [
		'labels'  => [
			'border-top-width'  => esc_html__( 'Top', 'yourtechtherapist' ),
			'border-bottom-width'  => esc_html__( 'Bottom', 'yourtechtherapist' ),
			'border-left-width' => esc_html__( 'Left', 'yourtechtherapist' ),
			'border-right-width' => esc_html__( 'Right', 'yourtechtherapist' ),
		],
	],
    'output'      => [
        'element'  => '.header_location header.primary > .secondary',
    ],
] );

// Secondary Header Container Width
Kirki::add_field( 'ytt', [
	'type'        => 'dimension',
	'settings'    => 'header_width',
	'label'       => esc_html__( 'Secondary Header Container Width', 'yourtechtherapist' ),
    'description'    => esc_html__( 'Adjust the width of the secondary header container.', 'yourtechtherapist' ),
	'section'     => 'secondary_header',
	'default'     => '100%',
	'priority'    => 100,
    'active_callback' => [
        [
            'setting'  => 'show_secondary_header',
            'operator' => '==',
            'value'    => true,
        ]
    ],
    'output'      => [
        'element' => 'header.primary .secondary',
        'property' => 'max-width',
    ],
] );

// Secondary Header Content Width
Kirki::add_field( 'ytt', [
	'type'        => 'dimension',
	'settings'    => 'header_content_width',
	'label'       => esc_html__( 'Secondary Header Content Width', 'yourtechtherapist' ),
    'description'    => esc_html__( 'Adjust the width of the secondary header content.', 'yourtechtherapist' ),
	'section'     => 'secondary_header',
	'default'     => '100%',
	'priority'    => 100,
    'active_callback' => [
        [
            'setting'  => 'show_secondary_header',
            'operator' => '==',
            'value'    => true,
        ]
    ],
    'output'      => [
        'element' => 'header.primary .secondary .wrapper',
        'property' => 'max-width',
    ],
] );

// Secondary Header Reverse Layout
Kirki::add_field( 'ytt', [
	'type'        => 'toggle',
	'settings'    => 'secondary_header_reverse_layout',
	'label'       => esc_html__( 'Secondary Header Reverse Layout', 'yourtechtherapist' ),
	'section'     => 'secondary_header',
    'active_callback' => [
        [
            'setting'  => 'show_secondary_header',
            'operator' => '==',
            'value'    => true,
        ]
    ],
	'default'     => 0,
] );


// Secondary Header Font
Kirki::add_field( 'ytt', [
    'type'        => 'typography',
    'settings'    => 'secondary_header_font',
    'label'       => esc_html__( 'Secondary Main Header Font', 'yourtechtherapist' ),
    'section'     => 'secondary_header',
    'theme_config' => 'yourtechtherapist',
    'active_callback' => [
        [
            'setting'  => 'show_secondary_header',
            'operator' => '==',
            'value'    => true,
        ]
    ],
    'default'     => [
        'font-family'    => 'Open Sans',
        'variant'        => 'regular',
        'font-size'      => '0.9em',
        'line-height'    => '1.5',
        'letter-spacing' => '0',
        'color'          => '#333333',
        'text-transform' => 'none',
        'text-align'     => 'center',
    ],
    'priority'    => 10,
    'transport'   => 'auto',
    'choices' => leedo_add_custom_choice(),
    'output'      => [ 
        'element' => 'header.primary > .secondary',
    ],
] );

// Secondary Header Link Color
Kirki::add_field( 'ytt', [
    'type'        => 'multicolor',
    'settings'    => 'secondary_header_link_colors',
    'label'       => esc_html__( 'Secondary Header Link Colors', 'yourtechtherapist' ),
    'section'     => 'secondary_header',
    'priority'    => 10,
    'active_callback' => [
        [
            'setting'  => 'show_secondary_header',
            'operator' => '==',
            'value'    => true,
        ]
    ],
    'choices'     => [
        'default'    => esc_html__( 'Normal', 'yourtechtherapist' ),
        'hover'   => esc_html__( 'Hover', 'yourtechtherapist' ),
        'visited'  => esc_html__( 'Visited', 'yourtechtherapist' ),
    ],
    'default'     => [
        'default'    => '#349BB3',
        'hover'   => '#74E6FF',
        'visited'  => '#74E6FF',
    ],
    'output'      => [
        [
            'choice' => 'default',
            'element' => ['header.primary > #secondaryheader a', 'header.primary > #secondaryheader li.fab'],
            'property' => 'color',
        ],
        [
            'choice' => 'visited',
            'element' => ['header.primary > #secondaryheader a:visited', 'header.primary > #secondaryheader li.fab:visited'],
            'property' => 'color',
        ],
        [
            'choice' => 'hover',
            'element' => ['header.primary > #secondaryheader a:hover', 'header.primary > #secondaryheader a:hover .has-text-color', 'header.primary > #secondaryheader li.fab:hover'],
            'property' => 'color',
        ],
    ],
] );