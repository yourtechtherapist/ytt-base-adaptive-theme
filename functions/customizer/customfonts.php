<?php

/***************************
//// 

CUSTOMIZER - CUSTOM FONT FUNCTION

Adds a custom function to include custom fonts, such as Adobe Typekit, in the Kirki Customizer Typography fields.

////
***************************/


/*****************************
//// CUSTOM FONT FUNCTION ////
*****************************/

if ( ! function_exists( 'leedo_add_custom_choice' ) ) {
	function leedo_add_custom_choice() {
		return array(
			'fonts' => apply_filters( 'vlthemes/kirki_font_choices', array() )
		);
	}
}