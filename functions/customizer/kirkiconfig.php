<?php

/***************************
//// 

CUSTOMIZER - KIRKI CONFIGURATION

Kirki Customizer Plugin configuration variables.

////
***************************/


// Early exit if Kirki doesn’t exist.
if ( ! class_exists( 'Kirki' ) ) {
    return;
}

Kirki::add_config( 'ytt', array(
	'capability' => 'edit_theme_options',
	'option_type' => 'theme_mod',
) );