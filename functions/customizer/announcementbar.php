<?php

/***************************
//// 

CUSTOMIZER - ANNOUNCEMENT BAR

Announcement Bar Settings in the WordPress Customizer.

////
***************************/


/**********************************
//// ANNOUNCEMENT BAR CONTROLS ////
**********************************/

// Kirki
// Announcement Bar Panel
Kirki::add_section( 'announcement_bar', array(
    'title'       => esc_html__( 'Announcement Bar', 'yourtechtherapist' ),
    'description' => esc_html__( 'Add an announcement bar to your website.', 'yourtechtherapist' ),
    'priority'    => 70,
) );

// Show Announcement Bar
Kirki::add_field( 'ytt', [
	'type'        => 'toggle',
	'settings'    => 'announcement_bar_toggle',
	'label'       => esc_html__( 'Show Announcement Bar', 'yourtechtherapist' ),
	'section'     => 'announcement_bar',
	'default'     => 0,
] );

// Show Announcement Bar On All Pages
Kirki::add_field( 'ytt', [
	'type'        => 'toggle',
	'settings'    => 'announcement_bar_all_pages',
	'label'       => esc_html__( 'Show Announcement Bar On All Pages', 'yourtechtherapist' ),
    'description' => esc_html__( 'Announcement bar will only display on the home page when this is turned OFF.', 'yourtechtherapist' ),
	'section'     => 'announcement_bar',
	'default'     => 1,
] );

// Announcement Bar Background Color
Kirki::add_field( 'ytt', [
	'type'        => 'color',
	'settings'    => 'announcement_bar_background',
	'label'       => __( 'Announcement Bar Background Color', 'yourtechtherapist' ),
	'section'     => 'announcement_bar',
	'default'     => '#cc3030',
    'active_callback' => [
        [
            'setting'  => 'announcement_bar_toggle',
            'operator' => '==',
            'value'    => true,
        ]
    ],
    'output'      => [
        'element' => '#announcementbar',
        'property' => 'background-color',
    ],
] );

// Announcement Bar Font Color
Kirki::add_field( 'ytt', [
	'type'        => 'color',
	'settings'    => 'announcement_bar_font',
	'label'       => __( 'Announcement Bar Font Color', 'yourtechtherapist' ),
	'section'     => 'announcement_bar',
	'default'     => '#ffffff',
    'active_callback' => [
        [
            'setting'  => 'announcement_bar_toggle',
            'operator' => '==',
            'value'    => true,
        ]
    ],
    'output'      => [
        'element' => ['#announcementbar', '#announcementbar a'],
        'property' => 'color',
    ],
] );

Kirki::add_field( 'ytt', [
	'type'        => 'editor',
	'settings'    => 'announcement_bar_content',
	'label'       => esc_html__( 'Announcement Bar Content', 'yourtechtherapist' ),
	'section'     => 'announcement_bar',
	'default'     => 'This is your announcement bar content.',
    'active_callback' => [
        [
            'setting'  => 'announcement_bar_toggle',
            'operator' => '==',
            'value'    => true,
        ]
    ],
] );