<?php

/***************************
//// 

CUSTOMIZER - ADDITIONAL CODE

Additional code settings in the WordPress Customizer.

////
***************************/


/*********************************
//// ADDITIONAL CODE CONTROLS ////
*********************************/

// Kirki
// Announcement Bar Panel
Kirki::add_section( 'additional_code', array(
    'title'       => esc_html__( 'Additional Code', 'yourtechtherapist' ),
    'description' => esc_html__( 'Add code snippets to the <head>, <body> or footer.', 'yourtechtherapist' ),
    'priority'    => 900,
) );

// <head>
Kirki::add_field( 'ytt', [
    'type'        => 'code',
    'settings'    => 'head_code',
    'label'       => esc_html__( 'Head Code', 'yourtechtherapist' ),
    'description' => esc_html__( 'This code will be added to the <head> of the website.', 'yourtechtherapist' ),
    'section'     => 'additional_code',
    'default'     => '',
    'priority'    => 10,
]);

// <body>
Kirki::add_field( 'ytt', [
    'type'        => 'code',
    'settings'    => 'body_code',
    'label'       => esc_html__( 'Body Code', 'yourtechtherapist' ),
    'description' => esc_html__( 'This code will be added at the beginning of the <body> of the website.', 'yourtechtherapist' ),
    'section'     => 'additional_code',
    'default'     => '',
    'priority'    => 10,
]);

// <body>
Kirki::add_field( 'ytt', [
    'type'        => 'code',
    'settings'    => 'footer_code',
    'label'       => esc_html__( 'Footer Code', 'yourtechtherapist' ),
    'description' => esc_html__( 'This code will be added in the footer of the website', 'yourtechtherapist' ),
    'section'     => 'additional_code',
    'default'     => '',
    'priority'    => 10,
]);