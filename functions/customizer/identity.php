<?php

/***************************
//// 

CUSTOMIZER - IDENTITY

IDentity Settings in the WordPress Customizer.

////
***************************/


/****************************
//// CUSTOMIZER CONTROLS ////
****************************/


// Kirki 

// Logo Height
Kirki::add_field( 'ytt', [
	'type'        => 'dimension',
	'settings'    => 'logo_height',
	'label'       => esc_html__( 'Logo Height', 'yourtechtherapist' ),
	'description' => esc_html__( 'Define the size of the logo using any valid CSS unit(example 5em, 10px, 2vh). This will control the height of the logo, the width will automatically scale.', 'yourtechtherapist' ),
	'section'     => 'title_tagline',
	'default'     => '5em',
	'priority'    => 9,
    'output'      => [
        [
            'element'  => 'header.primary .custom-logo-link img',
            'property' => 'height',
        ],
        [
            'element'  => 'header.primary.sticky_header.scroll .custom-logo-link img',
            'property' => 'height',
            'value_pattern' => 'calc($ / 2)'
        ],
    ],
] );

// Logo Height - Mobile
Kirki::add_field( 'ytt', [
	'type'        => 'dimension',
	'settings'    => 'logo_height_mobile',
	'label'       => esc_html__( 'Logo Height - Mobile', 'yourtechtherapist' ),
	'description' => esc_html__( 'Define the size of the logo on mobile using any valid CSS unit(example 5em, 10px, 2vh). This will control the height of the logo, the width will automatically scale.', 'yourtechtherapist' ),
	'section'     => 'title_tagline',
	'default'     => '2.5em',
	'priority'    => 9,
    'output'      => [
        'element'  => 'header.primary .custom-logo-link img',
        'property' => 'height',
        'media_query' => '@media (max-width: 768px)',
    ],
] );

// Show Site Title
Kirki::add_field( 'ytt', [
	'type'        => 'toggle',
	'settings'    => 'show_site_title',
	'label'       => esc_html__( 'Show Site Title', 'yourtechtherapist' ),
	'section'     => 'title_tagline',
	'default'     => '1',
	'priority'    => 11,
] );

// Site Title Font Size
Kirki::add_field( 'ytt', [
	'type'        => 'dimension',
	'settings'    => 'side_title_font_size',
	'label'       => esc_html__( 'Site Title Font Size', 'yourtechtherapist' ),
	'section'     => 'title_tagline',
	'default'     => '1.5em',
	'priority'    => 12,
    'active_callback' => [
        [
            'setting' => 'show_site_title',
            'operator' => '==',
            'value' => true,
        ],
    ],
    'output'      => [
        [
            'element' => 'header.primary .identity .site-title-link',
            'property' => 'font-size',
        ],
        [
            'element'  => 'header.primary.sticky_header.scroll .identity .site-title-link',
            'property' => 'font-size',
            'value_pattern' => 'calc($ / 1.5)'
        ],
    ],
] );

// Site Title Color
Kirki::add_field( 'ytt', [
	'type'        => 'color',
	'settings'    => 'site_title_color',
	'label'       => __( 'Site Title Color', 'yourtechtherapist' ),
	'section'     => 'title_tagline',
	'default'     => '#333333',
	'priority'    => 13,
    'active_callback' => [
        [
            'setting' => 'show_site_title',
            'operator' => '==',
            'value' => true,
        ],
    ],
    'output'      => [
        'element' => 'header.primary .identity .site-title-link h1',
        'property' => 'color',
    ],
] );

// Show Site Tagline
Kirki::add_field( 'ytt', [
	'type'        => 'toggle',
	'settings'    => 'show_site_tagline',
	'label'       => esc_html__( 'Show Site Tagline', 'yourtechtherapist' ),
	'section'     => 'title_tagline',
	'default'     => '1',
	'priority'    => 14,
] );

// Site Tagline Font Size
Kirki::add_field( 'ytt', [
	'type'        => 'dimension',
	'settings'    => 'side_tagline_font_size',
	'label'       => esc_html__( 'Site Tagline Font Size', 'yourtechtherapist' ),
	'section'     => 'title_tagline',
	'default'     => '0.85em',
	'priority'    => 15,
    'active_callback' => [
        [
            'setting' => 'show_site_tagline',
            'operator' => '==',
            'value' => true,
        ],
    ],
    'output'      => [
        [
            'element' => 'header.primary .identity .site-tagline-link',
            'property' => 'font-size',
        ],
        [
            'element'  => 'header.primary.sticky_header.scroll .identity .site-tagline-link',
            'property' => 'font-size',
            'value_pattern' => 'calc($ / 1.5)'
        ],
    ],
] );

// Site Tagline Color
Kirki::add_field( 'ytt', [
	'type'        => 'color',
	'settings'    => 'site_tagline_color',
	'label'       => __( 'Site Tagline Color', 'yourtechtherapist' ),
	'section'     => 'title_tagline',
	'default'     => '#333333',
	'priority'    => 16,
    'active_callback' => [
        [
            'setting' => 'show_site_tagline',
            'operator' => '==',
            'value' => true,
        ],
    ],
    'output'      => [
        'element' => 'header.primary .identity .site-tagline-link h2',
        'property' => 'color',
    ],
] );