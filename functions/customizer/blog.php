<?php

/***************************
//// 

CUSTOMIZER - BLOG

Blog Settings in the WordPress Customizer.

////
***************************/


/**********************
//// BLOG CONTROLS ////
**********************/

// Kirki

// Blog Callback
function blog_page_callback() {
    return ( is_home() || is_single() || is_archive() || is_search() ) ? true : false;
}

// Blog Panel
Kirki::add_panel( 'blog_panel', array(
    'priority'    => 160,
    'title'       => esc_html__( 'Blog', 'yourtechtherapist' ),
    'description' => esc_html__( 'Adjust your Blog settings.', 'yourtechtherapist' ),
) );

// Global Blog Settings
Kirki::add_section( 'blog_global', array(
    'title'          => esc_html__( 'Global Blog Settings', 'yourtechtherapist' ),
    'description'    => esc_html__( 'Adjust global blog settings.', 'yourtechtherapist' ),
    'panel'          => 'blog_panel',
    'priority'       => 60,
) );

// Post Background Color
Kirki::add_field( 'ytt', [
	'type'        => 'color',
	'settings'    => 'post_background_color',
	'label'       => __( 'Post Background Color', 'yourtechtherapist' ),
	'description' => esc_html__( 'Change the post background color.', 'yourtechtherapist' ),
	'section'     => 'blog_global',
	'default'     => 'rgba(255,255,255,0)',
	'choices'     => [
		'alpha' => true,
	],
    'active_callback' => 'blog_page_callback',
    'output'      => [
        'element' => '#container.index .post',
        'property' => 'background-color',
    ],
] );

// Entry Meta Background Color
Kirki::add_field( 'ytt', [
	'type'        => 'color',
	'settings'    => 'entry_meta_background_color',
	'label'       => __( 'Entry Meta Background Color', 'yourtechtherapist' ),
	'description' => esc_html__( 'Change the post entry meta background color.', 'yourtechtherapist' ),
	'section'     => 'blog_global',
	'default'     => 'rgba(81,81,81,0.50)',
	'choices'     => [
		'alpha' => true,
	],
    'active_callback' => 'blog_page_callback',
    'output'      => [
        'element' => ['#container.index .post .featured-image .entry-meta', '#container.index .post .featured-image .post-type span', '#container.index .post .content-wrapper .entry-utility span', '#container.single .post .entry-utility span'],
        'property' => 'background-color',
    ],
] );

// Entry Meta Text Color
Kirki::add_field( 'ytt', [
	'type'        => 'color',
	'settings'    => 'entry_meta_text_color',
	'label'       => __( 'Entry Meta Text Color', 'yourtechtherapist' ),
	'description' => esc_html__( 'Change the post entry meta text color.', 'yourtechtherapist' ),
	'section'     => 'blog_global',
	'default'     => 'rgba(255,255,255,1.0)',
	'choices'     => [
		'alpha' => true,
	],
    'active_callback' => 'blog_page_callback',
    'output'      => [
        'element' => ['#container.index .post .featured-image .entry-meta', '#container.index .post .featured-image .post-type span', '#container.index .post .content-wrapper .entry-utility span', '#container.index .post .content-wrapper .entry-utility span a', '#container.single .post .entry-utility span', '#container.single .post .entry-utility span a'],
        'property' => 'color',
    ],
] );

// Entry Meta Text Color
Kirki::add_field( 'ytt', [
	'type'        => 'color',
	'settings'    => 'entry_meta_text_hover_color',
	'label'       => __( 'Entry Meta Text Hover Color', 'yourtechtherapist' ),
	'description' => esc_html__( 'Change the post entry meta text hover color.', 'yourtechtherapist' ),
	'section'     => 'blog_global',
	'default'     => 'rgba(255,255,255,1.0)',
	'choices'     => [
		'alpha' => true,
	],
    'active_callback' => 'blog_page_callback',
    'output'      => [
        'element' => ['#container.index .post .content-wrapper .entry-utility span a:hover', '#container.single .post .entry-utility span a:hover'],
        'property' => 'color',
    ],
] );

// Post Border Width
Kirki::add_field( 'ytt', [
	'type'        => 'slider',
	'settings'    => 'post_border_width',
	'label'       => esc_html__( 'Post Border Width', 'yourtechtherapist' ),
	'section'     => 'blog_global',
	'default'     => '0.1',
	'choices'     => [
		'min'  => 0,
		'max'  => 1,
		'step' => 0.05,
	],
    'active_callback' => 'blog_page_callback',
    'output'      => [
        [
            'element' => '#container.index .post',
            'property' => 'border-width',
            'units' => 'rem',
        ],
        [
            'element' => '#container.index .post .featured-image .entry-meta',
            'property' => 'border-bottom-width',
            'units' => 'rem',
        ],
        [
            'element' => '#container.index .post .featured-image .entry-meta',
            'property' => 'border-right-width',
            'units' => 'rem',
        ],
        [
            'element' => '#container.index .post .featured-image .post-type span',
            'property' => 'border-bottom-width',
            'units' => 'rem',
        ],
        [
            'element' => '#container.index .post .featured-image .post-type span',
            'property' => 'border-left-width',
            'units' => 'rem',
        ],
    ],
] );

// Post Border Color
Kirki::add_field( 'ytt', [
	'type'        => 'color',
	'settings'    => 'post_border_color',
	'label'       => __( 'Post Border Color', 'yourtechtherapist' ),
	'description' => esc_html__( 'Change the post background color.', 'yourtechtherapist' ),
	'section'     => 'blog_global',
	'default'     => 'rgba(255,255,255,0)',
	'choices'     => [
		'alpha' => true,
	],
    'active_callback' => 'blog_page_callback',
    'output'      => [
        'element' => ['#container.index .post', '#container.index .post .featured-image .post-type span', '#container.index .post .featured-image .entry-meta'],
        'property' => 'border-color',
    ],
] );

// Post Border Radius
Kirki::add_field( 'ytt', [
	'type'        => 'slider',
	'settings'    => 'post_border_radius',
	'label'       => esc_html__( 'Post Border Radius', 'yourtechtherapist' ),
	'section'     => 'blog_global',
	'default'     => '0.25',
	'choices'     => [
		'min'  => 0,
		'max'  => 5,
		'step' => 0.05,
	],
    'active_callback' => 'blog_page_callback',
    'output'      => [
        [
            'element' => ['#container.index .post', '#container.index .post .content-wrapper .entry-utility span', '#container.single .post .entry-utility span'],
            'property' => 'border-radius',
            'units' => 'rem',
        ],
        [
            'element' => ['#container.index .post .featured-image', '#container.index .post .featured-image > img', '#container.index .post .featured-image .entry-meta'],
            'property' => 'border-top-left-radius',
            'units' => 'rem',
        ],
        [
            'element' => ['#container.index .post .featured-image', '#container.index .post .featured-image > img', '#container.index .post .featured-image .post-type span'],
            'property' => 'border-top-right-radius',
            'units' => 'rem',
        ],
        [
            'element' => '#container.index .post .featured-image .entry-meta',
            'property' => 'border-bottom-right-radius',
            'units' => 'rem',
        ],
        [
            'element' => '#container.index .post .featured-image .post-type span',
            'property' => 'border-bottom-left-radius',
            'units' => 'rem',
        ],
    ],
] );

// Post Text Alignment
Kirki::add_field( 'ytt', [
    'type'        => 'typography',
    'settings'    => 'post_text_alignment',
    'label'       => esc_html__( 'Post Text Alignment', 'yourtechtherapist' ),
    'section'     => 'blog_global',
    'theme_config' => 'yourtechtherapist',
    'default'     => [
        'text-align'     => 'left',
    ],
    'active_callback' => 'blog_page_callback',
    'priority'    => 10,
    'transport'   => 'auto',
    'output'      => [
        'element' => '#container.index .post .wrapper',
    ],
] );

// Blog Index
Kirki::add_section( 'blog_index', array(
    'title'          => esc_html__( 'Blog Index', 'yourtechtherapist' ),
    'description'    => esc_html__( 'Adjust blog index specific settings.', 'yourtechtherapist' ),
    'panel'          => 'blog_panel',
    'priority'       => 70,
) );

// Blog Posts Per Page
Kirki::add_field( 'ytt', [
    'type'        => 'number',
    'settings'    => 'blog_posts_per_page',
    'label'       => __( 'Posts Per Page.', 'yourtechtherapist' ),
    'description' => esc_html__( 'Change the number of posts that are shown per page on the main Blog index.', 'yourtechtherapist' ),
    'section'     => 'blog_index',
    'default'     => '6',
    'active_callback' => 'blog_page_callback',
    'choices'     => [
        'min' => 1,
        'max' => 10,
        'step' => 1,
    ],
] );

// Show full content on index
Kirki::add_field( 'ytt', [
    'type'        => 'toggle',
    'settings'    => 'show_full_post',
    'label'       => __( 'Show Full Post', 'yourtechtherapist' ),
    'description' => esc_html__( 'Show the full post content instead of an excerpt on the main index.', 'yourtechtherapist' ),
    'section'     => 'blog_index',
    'default'     => '0',
    'active_callback' => 'blog_page_callback',
] );

// Blog Layout
Kirki::add_field( 'ytt', [
    'type'        => 'select',
    'settings'    => 'blog_layout',
    'label'       => __( 'Blog Layout.', 'yourtechtherapist' ),
    'section'     => 'blog_index',
    'default'     => 'row',
	'placeholder' => esc_html__( 'Select an option...', 'yourtechtherapist' ),
	'multiple'    => 1,
    'active_callback' => 'blog_page_callback',
	'choices'     => [
		'row' => esc_html__( 'Row', 'yourtechtherapist' ),
		'grid' => esc_html__( 'Grid', 'yourtechtherapist' ),
	],
] );

// Archive Index
Kirki::add_section( 'archive_index', array(
    'title'          => esc_html__( 'Archive Index', 'yourtechtherapist' ),
    'description'    => esc_html__( 'Adjust main archive specific settings (date archives, categories, tags, author, etc).', 'yourtechtherapist' ),
    'panel'          => 'blog_panel',
    'priority'       => 70,
) );

// Archive Posts Per Page
Kirki::add_field( 'ytt', [
    'type'        => 'number',
    'settings'    => 'archive_posts_per_page',
    'label'       => __( 'Posts Per Page.', 'yourtechtherapist' ),
    'description' => esc_html__( 'Change the number of posts that are shown per page on the Archive index.', 'yourtechtherapist' ),
    'section'     => 'archive_index',
    'default'     => '6',
    'active_callback' => 'blog_page_callback',
    'choices'     => [
        'min' => 1,
        'max' => 10,
        'step' => 1,
    ],
] );

// Search Index
Kirki::add_section( 'search_index', array(
    'title'          => esc_html__( 'Search Index', 'yourtechtherapist' ),
    'description'    => esc_html__( 'Adjust search specific settings.', 'yourtechtherapist' ),
    'panel'          => 'blog_panel',
    'priority'       => 70,
) );

// Search Posts Per Page
Kirki::add_field( 'ytt', [
    'type'        => 'number',
    'settings'    => 'search_posts_per_page',
    'label'       => __( 'Posts Per Page.', 'yourtechtherapist' ),
    'description' => esc_html__( 'Change the number of posts that are shown per page on the Search index.', 'yourtechtherapist' ),
    'section'     => 'search_index',
    'default'     => '6',
    'active_callback' => 'blog_page_callback',
    'choices'     => [
        'min' => 1,
        'max' => 10,
        'step' => 1,
    ],
] );

// Search Layout
Kirki::add_field( 'ytt', [
    'type'        => 'select',
    'settings'    => 'search_layout',
    'label'       => __( 'Search Layout.', 'yourtechtherapist' ),
    'section'     => 'search_index',
    'default'     => 'row',
	'placeholder' => esc_html__( 'Select an option...', 'yourtechtherapist' ),
	'multiple'    => 1,
    'active_callback' => 'blog_page_callback',
	'choices'     => [
		'row' => esc_html__( 'Row', 'yourtechtherapist' ),
		'grid' => esc_html__( 'Grid', 'yourtechtherapist' ),
	],
] );

// Single Post
Kirki::add_section( 'single_post', array(
    'title'          => esc_html__( 'Single Post', 'yourtechtherapist' ),
    'description'    => esc_html__( 'Adjust the single post settings.', 'yourtechtherapist' ),
    'panel'          => 'blog_panel',
    'priority'       => 70,
) );

// Single Post Background Color
Kirki::add_field( 'ytt', [
	'type'        => 'color',
	'settings'    => 'single_post_background_color',
	'label'       => __( 'Post Background Color', 'yourtechtherapist' ),
	'description' => esc_html__( 'Change the post background color.', 'yourtechtherapist' ),
	'section'     => 'single_post',
	'default'     => 'rgba(255,255,255,0)',
	'choices'     => [
		'alpha' => true,
	],
    'active_callback' => 'blog_page_callback',
    'output'      => [
        'element' => '#container.single .post',
        'property' => 'background-color',
    ],
] );

// Single Post Title Alignment
Kirki::add_field( 'ytt', [
    'type'        => 'typography',
    'settings'    => 'single_post_title_alignment',
    'label'       => esc_html__( 'Single Post Title Alignment', 'yourtechtherapist' ),
    'section'     => 'single_post',
    'theme_config' => 'yourtechtherapist',
    'default'     => [
        'text-align'     => 'center',
    ],
    'active_callback' => 'blog_page_callback',
    'priority'    => 10,
    'transport'   => 'auto',
    'output'      => [
        'element' => '#container.single .entry-title',
    ],
] );

// Single Post Text Alignment
Kirki::add_field( 'ytt', [
    'type'        => 'typography',
    'settings'    => 'single_post_text_alignment',
    'label'       => esc_html__( 'Single Post Text Alignment', 'yourtechtherapist' ),
    'section'     => 'single_post',
    'theme_config' => 'yourtechtherapist',
    'default'     => [
        'text-align'     => 'left',
    ],
    'active_callback' => 'blog_page_callback',
    'priority'    => 10,
    'transport'   => 'auto',
    'output'      => [
        'element' => '#container.single .post',
    ],
] );

// Comments Background Color
Kirki::add_field( 'ytt', [
	'type'        => 'color',
	'settings'    => 'comments_background_color',
	'label'       => __( 'Comments Background Color', 'yourtechtherapist' ),
	'description' => esc_html__( 'Change the comments background color.', 'yourtechtherapist' ),
	'section'     => 'single_post',
	'default'     => 'rgba(255,255,255,0)',
	'choices'     => [
		'alpha' => true,
	],
    'active_callback' => 'blog_page_callback',
    'output'      => [
        'element' => ['#container.single #comment-list', '#container.single #respond'],
        'property' => 'background-color',
    ],
] );

// Single Comment Background Color
Kirki::add_field( 'ytt', [
	'type'        => 'color',
	'settings'    => 'single_comment_background_color',
	'label'       => __( 'Single Comment Background Color', 'yourtechtherapist' ),
	'description' => esc_html__( 'Change all single comments background color.', 'yourtechtherapist' ),
	'section'     => 'single_post',
	'default'     => 'rgba(255,255,255,0)',
	'choices'     => [
		'alpha' => true,
	],
    'active_callback' => 'blog_page_callback',
    'output'      => [
        'element' => '#container.single #comment-list .comment',
        'property' => 'background-color',
    ],
] );