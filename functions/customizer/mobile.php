<?php

/***************************
//// 

CUSTOMIZER - MOBILE

Mobile Settings in the WordPress Customizer.

////
***************************/


/************************
//// MOBILE CONTROLS ////
************************/

// Kirki

// Mobile Panel
Kirki::add_panel( 'mobile_panel', array(
    'priority'    => 160,
    'title'       => esc_html__( 'Mobile', 'yourtechtherapist' ),
    'description' => esc_html__( 'Adjust settings for mobile devices.', 'yourtechtherapist' ),
) );


// Mobile Menu Section
Kirki::add_section( 'mobile_menu', array(
    'title'          => esc_html__( 'Mobile Menu', 'yourtechtherapist' ),
    'description'    => esc_html__( 'Mobile navigation menu settings.', 'yourtechtherapist' ),
    'panel'          => 'mobile_panel',
    'priority'       => 70,
) );

// Mobile Menu Background Color
Kirki::add_field( 'ytt', [
	'type'        => 'color',
	'settings'    => 'mobile_menu_background',
	'label'       => __( 'Mobile Menu Background', 'yourtechtherapist' ),
	'description' => esc_html__( 'Change the background color of the mobile flyout menu.', 'yourtechtherapist' ),
	'section'     => 'mobile_menu',
	'default'     => '#349BB3',
    'output'      => [
        'element'  => ['header.primary nav#nav', 'header.primary nav#nav ul li.page_item_has_children ul', 'header.primary nav#nav ul li.menu-item-has-children ul', 'header.primary nav#nav .searchform'],
        'property' => 'background-color',
        'media_query' => '@media (max-width: 768px)',
    ],
] );

// Mobile Menu Font
Kirki::add_field( 'ytt', [
    'type'        => 'typography',
    'settings'    => 'mobile_menu_font',
    'label'       => esc_html__( 'Mobile Menu Font', 'yourtechtherapist' ),
    'section'     => 'mobile_menu',
    'theme_config' => 'yourtechtherapist',
    'default'     => [
        'font-family'    => 'Open Sans',
        'variant'        => 'regular',
        'line-height'    => '1.5',
        'letter-spacing' => '0',
        'color'          => '#ffffff',
        'text-transform' => 'none',
    ],
    'priority'    => 10,
    'transport'   => 'auto',
    'choices' => leedo_add_custom_choice(),
    'output'      => [
        'element'  => ['header.primary nav#nav', 'header.primary nav#nav ul li.page_item_has_children ul', 'header.primary nav#nav ul li.menu-item-has-children ul', 'header.primary nav#nav .searchform'],
        'media_query' => '@media (max-width: 768px)',
    ],
] );

// Mobile Menu Link Color
Kirki::add_field( 'ytt', [
    'type'        => 'multicolor',
    'settings'    => 'mobile_menu_link_colors',
    'label'       => esc_html__( 'Mobile Menu Link Colors', 'yourtechtherapist' ),
    'section'     => 'mobile_menu',
    'priority'    => 10,
    'choices'     => [
        'default'    => esc_html__( 'Normal', 'yourtechtherapist' ),
        'hover'   => esc_html__( 'Hover', 'yourtechtherapist' ),
        'visited'  => esc_html__( 'Visited', 'yourtechtherapist' ),
    ],
    'default'     => [
        'default'    => '#349BB3',
        'hover'   => '#74E6FF',
        'visited'  => '#74E6FF',
    ],    'output'      => [
        [
            'choice' => 'default',
            'element' => 'header.primary nav#nav a',
            'property' => 'color',
            'media_query' => '@media (max-width: 768px)',
        ],
        [
            'choice' => 'visited',
            'element' => 'header.primary nav#nav a:visited',
            'property' => 'color',
            'media_query' => '@media (max-width: 768px)',
        ],
        [
            'choice' => 'hover',
            'element' => ['header.primary nav#nav a:hover', 'header.primary nav#nav a:hover .has-text-color'],
            'property' => 'color',
            'media_query' => '@media (max-width: 768px)',
        ],
    ],
] );