<?php

/***************************
//// 

CUSTOMIZER - FONTS

Font Settings in the WordPress Customizer.

////
***************************/
    
    
/**********************
//// FONT SETTINGS ////
**********************/

// Kirki

Kirki::add_section( 'fonts', array(
    'title'          => esc_html__( 'Fonts', 'yourtechtherapist' ),
    'description'    => esc_html__( 'Font settings', 'yourtechtherapist' ),
    'priority'       => 70,
) );

// Base Font Size
Kirki::add_field( 'ytt', [
	'type'        => 'dimension',
	'settings'    => 'base_font_size',
	'label'       => esc_html__( 'Base Font Size', 'yourtechtherapist' ),
	'description' => esc_html__( 'Define the base font size for the website. This is applied to <body>. Must be a valid CSS unit.', 'yourtechtherapist' ),
	'section'     => 'fonts',
	'default'     => '1em',
    'output'      => [
        'element'  => 'body',
        'property' => 'font-size',
    ],
] );

// Footer Font Size
Kirki::add_field( 'ytt', [
	'type'        => 'dimension',
	'settings'    => 'footer_font_size',
	'label'       => esc_html__( 'Footer Font Size', 'yourtechtherapist' ),
	'description' => esc_html__( 'Define the font size for the website footer. Must be a valid CSS unit.', 'yourtechtherapist' ),
	'section'     => 'fonts',
	'default'     => '1em',
    'output'      => [
        'element'  => 'footer.primary',
        'property' => 'font-size',
    ],
] );

// Main Font
Kirki::add_field( 'ytt', [
    'type'        => 'typography',
    'settings'    => 'main_font',
    'label'       => esc_html__( 'Main Font', 'yourtechtherapist' ),
    'description' => esc_html__( 'Global base fonts settings.', 'yourtechtherapist' ),
    'section'     => 'fonts',
    'theme_config' => 'yourtechtherapist',
    'default'     => [
        'font-family'    => 'Open Sans',
        'variant'        => 'regular',
        'line-height'    => '1.5',
        'letter-spacing' => '0',
        'color'          => '#333333',
        'text-transform' => 'none',
        'text-align'     => 'center',
    ],
    'priority'    => 10,
    'transport'   => 'auto',
    'choices' => leedo_add_custom_choice(),
    'output'      => [
        [
            'element' => ['body', '.font-color'],
        ],
        [
            'element' => '.edit-post-visual-editor.editor-styles-wrapper', 
            'context' => [ 'editor' ],
        ],
    ],
] );

// Heading Font
Kirki::add_field( 'ytt', [
    'type'        => 'typography',
    'settings'    => 'heading_font',
    'label'       => esc_html__( 'Heading Font', 'yourtechtherapist' ),
    'description' => esc_html__( 'Font settings for all headings.', 'yourtechtherapist' ),
    'section'     => 'fonts',
    'default'     => [
        'font-family'    => 'Open Sans',
        'variant'        => 'regular',
        'line-height'    => '1.5',
        'letter-spacing' => '0',
        'color'          => '#333333',
        'text-transform' => 'none',
        'text-align'     => 'center',
    ],
    'priority'    => 10,
    'transport'   => 'auto',
    'choices' => leedo_add_custom_choice(),
    'output'      => [
        [
            'element' => array('h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'h1 a', 'h2 a', 'h3 a', 'h4 a', 'h5 a', 'h6 a', 'h1 a:visited', 'h2 a:visited', 'h3 a:visited', 'h4 a:visited', 'h5 a:visited', 'h6 a:visited'),
        ],
        [
            'element' => array('.edit-post-visual-editor.editor-styles-wrapper h1', '.edit-post-visual-editor.editor-styles-wrapper h2', '.edit-post-visual-editor.editor-styles-wrapper h3', '.edit-post-visual-editor.editor-styles-wrapper h4', '.edit-post-visual-editor.editor-styles-wrapper h5', '.edit-post-visual-editor.editor-styles-wrapper h6'),
            'context' => [ 'editor' ],
        ],
    ],
] );

// Alt Heading Font
Kirki::add_field( 'ytt', [
    'type'        => 'typography',
    'settings'    => 'alt_heading_font',
    'label'       => esc_html__( 'Alt Heading Font', 'yourtechtherapist' ),
    'description' => esc_html__( 'Alt heading font settings. To use, select Alt on your heading in the Gutenberg page editor.', 'yourtechtherapist' ),
    'section'     => 'fonts',
    'default'     => [
        'font-family'    => 'Open Sans',
        'variant'        => 'regular',
        'line-height'    => '1.5',
        'letter-spacing' => '0',
        'color'          => '#333333',
        'text-transform' => 'none',
        'text-align'     => 'center',
    ],
    'priority'    => 10,
    'transport'   => 'auto',
    'choices' => leedo_add_custom_choice(),
    'output'      => [
        [
            'element' => ['.is-style-alt', '.is-style-alt a', '.is-style-alt a:visited'],
        ],
        [
            'element' => ['.edit-post-visual-editor.editor-styles-wrapper .is-style-alt', '.edit-post-visual-editor.editor-styles-wrapper .is-style-alt a', '.edit-post-visual-editor.editor-styles-wrapper .is-style-alt a:visited'],
            'context' => [ 'editor' ],
        ],
    ],
] );

// Heading 1 Font Size
Kirki::add_field( 'ytt', [
	'type'        => 'dimension',
	'settings'    => 'h1_font_size',
	'label'       => esc_html__( 'Heading 1 Font Size', 'yourtechtherapist' ),
	'description' => esc_html__( 'Define the font size for <h1> tags. Must be a valid CSS unit.', 'yourtechtherapist' ),
	'section'     => 'fonts',
	'default'     => '3em',
    'output'      => [
        'element'  => 'h1',
        'property' => 'font-size',
    ],
] );

// Heading 2 Font Size
Kirki::add_field( 'ytt', [
	'type'        => 'dimension',
	'settings'    => 'h2_font_size',
	'label'       => esc_html__( 'Heading 2 Font Size', 'yourtechtherapist' ),
	'description' => esc_html__( 'Define the font size for <h2> tags. Must be a valid CSS unit.', 'yourtechtherapist' ),
	'section'     => 'fonts',
	'default'     => '2.25em',
    'output'      => [
        'element'  => 'h2',
        'property' => 'font-size',
    ],
] );

// Heading 3 Font Size
Kirki::add_field( 'ytt', [
	'type'        => 'dimension',
	'settings'    => 'h3_font_size',
	'label'       => esc_html__( 'Heading 3 Font Size', 'yourtechtherapist' ),
	'description' => esc_html__( 'Define the font size for <h3> tags. Must be a valid CSS unit.', 'yourtechtherapist' ),
	'section'     => 'fonts',
	'default'     => '1.75em',
    'output'      => [
        'element'  => 'h3',
        'property' => 'font-size',
    ],
] );

// Heading 4 Font Size
Kirki::add_field( 'ytt', [
	'type'        => 'dimension',
	'settings'    => 'h4_font_size',
	'label'       => esc_html__( 'Heading 4 Font Size', 'yourtechtherapist' ),
	'description' => esc_html__( 'Define the font size for <h4> tags. Must be a valid CSS unit.', 'yourtechtherapist' ),
	'section'     => 'fonts',
	'default'     => '1.5em',
    'output'      => [
        'element'  => 'h4',
        'property' => 'font-size',
    ],
] );

// Heading 5 Font Size
Kirki::add_field( 'ytt', [
	'type'        => 'dimension',
	'settings'    => 'h5_font_size',
	'label'       => esc_html__( 'Heading 5 Font Size', 'yourtechtherapist' ),
	'description' => esc_html__( 'Define the font size for <h5> tags. Must be a valid CSS unit.', 'yourtechtherapist' ),
	'section'     => 'fonts',
	'default'     => '1.25em',
    'output'      => [
        'element'  => 'h5',
        'property' => 'font-size',
    ],
] );

// Heading 6 Font Size
Kirki::add_field( 'ytt', [
	'type'        => 'dimension',
	'settings'    => 'h6_font_size',
	'label'       => esc_html__( 'Heading 6 Font Size', 'yourtechtherapist' ),
	'description' => esc_html__( 'Define the font size for <h6> tags. Must be a valid CSS unit.', 'yourtechtherapist' ),
	'section'     => 'fonts',
	'default'     => '1.1em',
    'output'      => [
        'element'  => 'h6',
        'property' => 'font-size',
    ],
] );

// Link Color
Kirki::add_field( 'ytt', [
    'type'        => 'multicolor',
    'settings'    => 'link_colors',
    'label'       => esc_html__( 'Link Colors', 'kirki' ),
	'description' => esc_html__( 'Change text color for all links in the content and footer.', 'yourtechtherapist' ),
    'section'     => 'fonts',
    'priority'    => 10,
    'choices'     => [
        'default'    => esc_html__( 'Normal', 'kirki' ),
        'hover'   => esc_html__( 'Hover', 'kirki' ),
        'visited'  => esc_html__( 'Visited', 'kirki' ),
    ],
    'default'     => [
        'default'    => '#349BB3',
        'hover'   => '#74E6FF',
        'visited'  => '#74E6FF',
    ],
    'output'      => [
        [
            'choice' => 'default',
            'element' => 'a:not(.button):not(.wp-block-button__link)',
            'property' => 'color',
        ],
        [
            'choice' => 'visited',
            'element' => 'a:not(.button):not(.wp-block-button__link):visited',
            'property' => 'color',
        ],
        [
            'choice' => 'hover',
            'element' => ['a:not(.button):not(.wp-block-button__link):hover', 'a:not(.button):not(.wp-block-button__link):hover .has-text-color'],
            'property' => 'color',
        ],
    ],
] );