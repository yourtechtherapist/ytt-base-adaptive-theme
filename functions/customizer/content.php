<?php

/***************************
//// 

CUSTOMIZER - CONTENT

COntent Settings in the WordPress Customizer.

////
***************************/

// Remove WP Default Background Color
function remove_customizer_settings() {     
global $wp_customize;
$wp_customize->remove_control( 'background_color' );  //Modify this line as needed  
} 

add_action( 'customize_register', 'remove_customizer_settings', 11 );

// Kirki

// Content Panel
Kirki::add_panel( 'content_panel', array(
    'priority'    => 160,
    'title'       => esc_html__( 'Content', 'yourtechtherapist' ),
    'description' => esc_html__( 'Adjust content settings', 'yourtechtherapist' ),
) );

// Content Color Section
Kirki::add_section( 'content_color', array(
    'title'          => esc_html__( 'Content Color', 'yourtechtherapist' ),
    'description'    => esc_html__( 'Adjust content colors.', 'yourtechtherapist' ),
    'panel'          => 'content_panel',
    'priority'       => 70,
) );

// Page Background Color
Kirki::add_field( 'ytt', [
	'type'        => 'color',
	'settings'    => 'page_background',
	'label'       => __( 'Page Background Color', 'yourtechtherapist' ),
	'description' => esc_html__( 'Change the page background color.', 'yourtechtherapist' ),
	'section'     => 'content_color',
	'default'     => '#FFFFFF',
    'output'      => [
        'element' => 'body',
        'property' => 'background-color',
    ],
] );

// Content Background Color
Kirki::add_field( 'ytt', [
	'type'        => 'color',
	'settings'    => 'content_background',
	'label'       => __( 'Content Background Color', 'yourtechtherapist' ),
	'description' => esc_html__( 'Change the content background color.', 'yourtechtherapist' ),
	'section'     => 'content_color',
	'default'     => '#FFFFFF',
    'output'      => [
        'element' => 'main',
        'property' => 'background-color',
    ],
] );

// Content Layout Section
Kirki::add_section( 'content_layout', array(
    'title'          => esc_html__( 'Content Layout', 'yourtechtherapist' ),
    'description'    => esc_html__( 'Adjust content layout.', 'yourtechtherapist' ),
    'panel'          => 'content_panel',
    'priority'       => 70,
) );

// Content Container Width
Kirki::add_field( 'ytt', [
	'type'        => 'dimension',
	'settings'    => 'content_container_width',
	'label'       => esc_html__( 'Content Container Width', 'yourtechtherapist' ),
    'description'    => esc_html__( 'Adjust the width of the content container.', 'yourtechtherapist' ),
	'section'     => 'content_layout',
	'default'     => '100%',
	'priority'    => 10,
    'output'      => [
        [
            'element' => 'main',
            'property' => 'max-width',
        ],
        [
            'element' => '.edit-post-visual-editor.editor-styles-wrapper .block-editor-block-list__layout  .wp-block .wp-block[data-align="full"]',
            'property' => 'max-width',
            'context' => ['editor'],
        ],
    ],
] );

// Content Width
Kirki::add_field( 'ytt', [
	'type'        => 'dimension',
	'settings'    => 'content_width',
	'label'       => esc_html__( 'Content Width', 'yourtechtherapist' ),
    'description'    => esc_html__( 'Adjust the width of the content.', 'yourtechtherapist' ),
	'section'     => 'content_layout',
	'default'     => '63.15rem',
	'priority'    => 10,
    'output'      => [
        [
            'element' => ['main #container section > *', 'main #container section .alignwide, main #container section .alignfull .wp-block-group__inner-container', 'main #container section .alignfull .wp-block-getwid-section__inner-wrapper', 'main .content-width', 'main #container .content-width'],
            'property' => 'max-width',
        ],
        [
            'element' => ['.editor-post-title__block', '.editor-default-block-appender', '.editor-block-list__block', '.edit-post-visual-editor.editor-styles-wrapper .block-editor-block-list__layout  .wp-block.block-editor-block-list__block', '.edit-post-visual-editor.editor-styles-wrapper .block-editor-block-list__layout  .wp-block .wp-block[data-align="wide"]'],
            'property' => 'max-width',
            'context' => ['editor'],
            'value_pattern' => 'calc($ + 28px)',
        ],
    ],
] );

// Show Back To Top Button
Kirki::add_field( 'ytt', [
	'type'        => 'toggle',
	'settings'    => 'back_to_top_button',
	'label'       => esc_html__( 'Show Back To Top Button', 'yourtechtherapist' ),
	'description' => esc_html__( 'Display a back to top button in the bottom right corner on all pages.', 'yourtechtherapist' ),
	'section'     => 'content_layout',
	'default'     => 1,
] );

// Back To Top Button Location
Kirki::add_field( 'ytt', [
	'type'        => 'select',
	'settings'    => 'back_to_top_location',
	'label'       => esc_html__( 'Back To Top Button Location', 'yourtechtherapist' ),
	'section'     => 'content_layout',
    'active_callback' => [
        [
            'setting'  => 'back_to_top_button',
            'operator' => '==',
            'value'    => true,
        ]
    ],
	'placeholder' => esc_html__( 'Select an option...', 'yourtechtherapist' ),
	'default'     => 'right',
	'multiple'    => 1,
	'choices'     => [
		'right' => esc_html__( 'Right', 'yourtechtherapist' ),
		'left' => esc_html__( 'Left', 'yourtechtherapist' ),
	],
] );


// Buttons Section
// Content Layout Section
Kirki::add_section( 'button_defaults', array(
    'title'          => esc_html__( 'Buttons', 'yourtechtherapist' ),
    'description'    => esc_html__( 'Default layout and colors for all buttons.', 'yourtechtherapist' ),
    'panel'          => 'content_panel',
    'priority'       => 70,
) );

// Button Padding
Kirki::add_field( 'ytt', [
	'type'        => 'dimensions',
	'settings'    => 'button_padding',
	'label'       => esc_html__( 'Button Padding', 'yourtechtherapist' ),
	'section'     => 'button_defaults',
	'default'     => [
		'padding-top'    => '1rem',
		'padding-bottom' => '1rem',
		'padding-left'   => '1rem',
		'padding-right'  => '1rem',
    ],
	'choices'     => [
		'labels' => [
			'padding-top' => esc_html__( 'Top', 'yourtechtherapist' ),
			'padding-bottom'  => esc_html__( 'Bottom', 'yourtechtherapist' ),
			'padding-left' => esc_html__( 'Left', 'yourtechtherapist' ),
			'padding-right' => esc_html__( 'Right', 'yourtechtherapist' ),
		],
	],
    'output'      => [
        [
            'element' => ['button', 'input[type="submit"]', '.button', '.wp-block-button a', '.wp-block-uagb-cf7-styler .uagb-cf7-styler__btn-align-center input.wpcf7-form-control.wpcf7-submit', '.wpforms-form button', 'button.wpforms-submit'],
        ],
        [
            'element' => ['.edit-post-visual-editor.editor-styles-wrapper .block-editor-block-list__layout  .wp-block .wp-block-button .wp-block-button__link', '.edit-post-visual-editor.editor-styles-wrapper .block-editor-block-list__layout  .wp-block .button'],
            'context' => ['editor'],
        ],
    ],
] );

// Button Border Radius
Kirki::add_field( 'ytt', [
	'type'        => 'slider',
	'settings'    => 'button_border_radius',
	'label'       => esc_html__( 'Button Border Radius', 'yourtechtherapist' ),
	'section'     => 'button_defaults',
	'default'     => '0.25',
	'choices'     => [
		'min'  => 0,
		'max'  => 5,
		'step' => 0.05,
	],
    'output'      => [
        [
            'element' => ['button', 'input[type="submit"]', '.button', '.wp-block-button a', '.wp-block-uagb-cf7-styler .uagb-cf7-styler__btn-align-center input.wpcf7-form-control.wpcf7-submit', '.wpforms-form button', 'button.wpforms-submit'],
            'property' => 'border-radius',
            'units' => 'em',
        ],
        [
            'element' => ['.edit-post-visual-editor.editor-styles-wrapper .block-editor-block-list__layout  .wp-block .wp-block-button .wp-block-button__link', '.edit-post-visual-editor.editor-styles-wrapper .block-editor-block-list__layout  .wp-block .button'],
            'property' => 'border-radius',
            'units' => 'em',
            'context' => ['editor'],
        ],
    ],
] );

// Button Font
Kirki::add_field( 'ytt', [
    'type'        => 'typography',
    'settings'    => 'button_font',
    'label'       => esc_html__( 'Button Font', 'yourtechtherapist' ),
    'section'     => 'button_defaults',
    'theme_config' => 'yourtechtherapist',
    'default'     => [
        'font-family'    => 'Open Sans',
        'variant'        => 'regular',
        'line-height'    => '1.5',
        'letter-spacing' => '0',
        'text-transform' => 'none',
    ],
    'priority'    => 10,
    'transport'   => 'auto',
    'choices' => leedo_add_custom_choice(),
    'output'      => [
        [
            'element' => ['button', 'input[type="submit"]', '.button', '.wp-block-button a', '.wp-block-uagb-cf7-styler .uagb-cf7-styler__btn-align-center input.wpcf7-form-control.wpcf7-submit', '.wpforms-form button', 'button.wpforms-submit'],
        ],
    ],
] );

// Button Text Size
Kirki::add_field( 'ytt', [
	'type'        => 'dimension',
	'settings'    => 'button_text_size',
	'label'       => esc_html__( 'Button Text Size', 'yourtechtherapist' ),
	'section'     => 'button_defaults',
	'default'     => '1em',
    'output'      => [
        [
            'element' => ['button', 'input[type="submit"]', '.button', '.wp-block-button a', '.wp-block-uagb-cf7-styler .uagb-cf7-styler__btn-align-center input.wpcf7-form-control.wpcf7-submit', '.wpforms-form button', 'button.wpforms-submit'],
            'property' => 'font-size',
        ],
        [
            'element' => ['.edit-post-visual-editor.editor-styles-wrapper .block-editor-block-list__layout  .wp-block .wp-block-button .wp-block-button__link', '.edit-post-visual-editor.editor-styles-wrapper .block-editor-block-list__layout  .wp-block .button'],
            'property' => 'font-size',
            'context' => ['editor'],
        ],
    ],
] );

// Button Auto Swap On Hover
Kirki::add_field( 'ytt', [
	'type'        => 'toggle',
	'settings'    => 'button_hover_swap',
	'label'       => esc_html__( 'Button Color Swap', 'yourtechtherapist' ),
	'description' => esc_html__( 'Swap button colors on hover.', 'yourtechtherapist' ),
	'section'     => 'button_defaults',
	'default'     => 1,
] );

// Button Swap Colors
Kirki::add_field( 'ytt', [
    'type'        => 'multicolor',
    'settings'    => 'button_swap_colors',
    'label'       => esc_html__( 'Button Swap Colors', 'yourtechtherapist' ),
	'description' => esc_html__( 'Change the default color set for buttons when Button Color Swap is enabled.', 'yourtechtherapist' ),
    'section'     => 'button_defaults',
    'active_callback' => [
        [
            'setting'  => 'button_hover_swap',
            'operator' => '==',
            'value'    => true,
        ]
    ],
    'priority'    => 10,
    'choices'     => [
        'text'    => esc_html__( 'Text', 'yourtechtherapist' ),
        'background'   => esc_html__( 'Background', 'yourtechtherapist' ),
        'border'  => esc_html__( 'Border', 'yourtechtherapist' ),
    ],
    'default'     => [
        'text'    => '#FFFFFF',
        'background'   => '#349BB3',
        'border'  => '#349BB3',
    ],
    'output'      => [
        [
            'choice' => 'text',
            'element' => ['button', 'input[type="submit"]', '.button', 'a.button:visited', '.wp-block-button__link', 'input.wpcf7-form-control.wpcf7-submit', '.wpforms-form button', 'button.wpforms-submit'],
            'property' => 'color',
        ],
        [
            'choice' => 'text',
            'element' => ['.edit-post-visual-editor.editor-styles-wrapper .block-editor-block-list__layout  .wp-block .wp-block-button .wp-block-button__link', '.edit-post-visual-editor.editor-styles-wrapper .block-editor-block-list__layout  .wp-block .button'],
            'property' => 'color',
            'context' => ['editor'],
        ],
        [
            'choice' => 'background',
            'element' => ['button', 'input[type="submit"]', '.button', '.wp-block-button__link', 'input.wpcf7-form-control.wpcf7-submit', '.wpforms-form button', 'button.wpforms-submit'],
            'property' => 'background-color',
        ],
        [
            'choice' => 'background',
            'element' => ['.edit-post-visual-editor.editor-styles-wrapper .block-editor-block-list__layout  .wp-block .wp-block-button .wp-block-button__link', '.edit-post-visual-editor.editor-styles-wrapper .block-editor-block-list__layout  .wp-block .button'],
            'property' => 'background-color',
            'context' => ['editor'],
        ],
        [
            'choice' => 'border',
            'element' => ['button', 'input[type="submit"]', '.button', '.wp-block-button__link', 'input.wpcf7-form-control.wpcf7-submit', '.wpforms-form button', 'button.wpforms-submit'],
            'property' => 'border-color',
        ],
        [
            'choice' => 'border',
            'element' => ['.edit-post-visual-editor.editor-styles-wrapper .block-editor-block-list__layout  .wp-block .wp-block-button .wp-block-button__link', '.edit-post-visual-editor.editor-styles-wrapper .block-editor-block-list__layout  .wp-block .button'],
            'property' => 'border-color',
            'context' => ['editor'],
        ],
    ],
] );

// Button Text Color
Kirki::add_field( 'ytt', [
	'type'        => 'color',
	'settings'    => 'button_text',
	'label'       => __( 'Button Text Color.', 'yourtechtherapist' ),
	'description' => esc_html__( 'Change button text color.', 'yourtechtherapist' ),
	'section'     => 'button_defaults',
    'active_callback' => [
        [
            'setting'  => 'button_hover_swap',
            'operator' => '==',
            'value'    => false,
        ]
    ],
	'default'     => '#FFFFFF',
    'output'      => [
        [
            'element' => ['button', 'input[type="submit"]', '.button', 'a.button:visited', '.wp-block-button .wp-block-button__link', '.wp-block-uagb-cf7-styler .uagb-cf7-styler__btn-align-center input.wpcf7-form-control.wpcf7-submit', '.wpforms-form button', 'button.wpforms-submit'],
            'property' => 'color',
        ],
        [
            'element' => ['.edit-post-visual-editor.editor-styles-wrapper .block-editor-block-list__layout  .wp-block .wp-block-button .wp-block-button__link', '.edit-post-visual-editor.editor-styles-wrapper .block-editor-block-list__layout  .wp-block .button'],
            'property' => 'color',
            'context' => ['editor'],
        ],
    ],
	'choices'     => [
		'alpha' => true,
	],
] );

// Button Background Color
Kirki::add_field( 'ytt', [
	'type'        => 'color',
	'settings'    => 'button_background',
	'label'       => __( 'Button Background Color.', 'yourtechtherapist' ),
	'description' => esc_html__( 'Change button background color.', 'yourtechtherapist' ),
	'section'     => 'button_defaults',
    'active_callback' => [
        [
            'setting'  => 'button_hover_swap',
            'operator' => '==',
            'value'    => false,
        ]
    ],
	'default'     => '#349BB3',
    'output'      => [
        [
            'element' => ['button', 'input[type="submit"]', '.button', '.wp-block-button a', '.wp-block-uagb-cf7-styler .uagb-cf7-styler__btn-align-center input.wpcf7-form-control.wpcf7-submit', '.wpforms-form button', 'button.wpforms-submit'],
            'property' => 'background-color',
        ],
        [
            'element' => ['.edit-post-visual-editor.editor-styles-wrapper .block-editor-block-list__layout  .wp-block .wp-block-button .wp-block-button__link', '.edit-post-visual-editor.editor-styles-wrapper .block-editor-block-list__layout  .wp-block .button'],
            'property' => 'background-color',
            'context' => ['editor'],
        ],
    ],
	'choices'     => [
		'alpha' => true,
	],
] );

// Button Text Hover Color
Kirki::add_field( 'ytt', [
	'type'        => 'color',
	'settings'    => 'button_text_hover',
	'label'       => __( 'Button Text Hover Color.', 'yourtechtherapist' ),
	'description' => esc_html__( 'Change button text hover color.', 'yourtechtherapist' ),
	'section'     => 'button_defaults',
    'active_callback' => [
        [
            'setting'  => 'button_hover_swap',
            'operator' => '==',
            'value'    => false,
        ]
    ],
	'default'     => '#FFFFFF',
    'output'      => [
        [
            'element' => ['button:hover', 'input[type="submit"]:hover', '.button:hover', '.wp-block-button a:hover', '.wp-block-uagb-cf7-styler:hover', '.uagb-cf7-styler__btn-align-center:hover', 'input.wpcf7-form-control.wpcf7-submit:hover', '.wpforms-form button:hover', 'button.wpforms-submit:hover'],
            'property' => 'color',
            'suffix' => '!important',
        ],
        [
            'element' => ['.edit-post-visual-editor.editor-styles-wrapper .block-editor-block-list__layout  .wp-block .wp-block-button .wp-block-button__link:hover', '.edit-post-visual-editor.editor-styles-wrapper .block-editor-block-list__layout  .wp-block .button:hover'],
            'property' => 'color',
            'context' => ['editor'],
        ],
    ],
	'choices'     => [
		'alpha' => true,
	],
] );

// Button Background Hover Color
Kirki::add_field( 'ytt', [
	'type'        => 'color',
	'settings'    => 'button_background_hover',
	'label'       => __( 'Button Background Hover Color.', 'yourtechtherapist' ),
	'description' => esc_html__( 'Change button background hover color.', 'yourtechtherapist' ),
	'section'     => 'button_defaults',
    'active_callback' => [
        [
            'setting'  => 'button_hover_swap',
            'operator' => '==',
            'value'    => false,
        ]
    ],
	'default'     => '#349BB3',
    'output'      => [
        [
            'element' => ['button:hover', 'input[type="submit"]:hover', '.button:hover', '.wp-block-button a:hover', '.wp-block-uagb-cf7-styler:hover', '.uagb-cf7-styler__btn-align-center:hover', 'input.wpcf7-form-control.wpcf7-submit:hover', '.wpforms-form button:hover', 'button.wpforms-submit:hover'],
            'property' => 'background-color',
        ],
        [
            'element' => ['.edit-post-visual-editor.editor-styles-wrapper .block-editor-block-list__layout  .wp-block .wp-block-button .wp-block-button__link:hover', '.edit-post-visual-editor.editor-styles-wrapper .block-editor-block-list__layout  .wp-block .button:hover'],
            'property' => 'background-color',
            'context' => ['editor'],
        ],
    ],
	'choices'     => [
		'alpha' => true,
	],
] );

// Button Border Color
Kirki::add_field( 'ytt', [
	'type'        => 'color',
	'settings'    => 'button_border',
	'label'       => __( 'Button Border Color.', 'yourtechtherapist' ),
	'description' => esc_html__( 'Change button border color.', 'yourtechtherapist' ),
	'section'     => 'button_defaults',
    'active_callback' => [
        [
            'setting'  => 'button_hover_swap',
            'operator' => '==',
            'value'    => false,
        ]
    ],
	'default'     => '#349BB3',
    'output'      => [
        [
            'element' => ['button', 'input[type="submit"]', '.button', '.wp-block-button a', '.wp-block-uagb-cf7-styler .uagb-cf7-styler__btn-align-center input.wpcf7-form-control.wpcf7-submit', '.wpforms-form button', 'button.wpforms-submit'],
            'property' => 'border-color',
        ],
        [
            'element' => ['.edit-post-visual-editor.editor-styles-wrapper .block-editor-block-list__layout  .wp-block .wp-block-button .wp-block-button__link', '.edit-post-visual-editor.editor-styles-wrapper .block-editor-block-list__layout  .wp-block .button'],
            'property' => 'border-color',
            'context' => ['editor'],
        ],
    ],
	'choices'     => [
		'alpha' => true,
	],
] );

// Button Border Hover Color
Kirki::add_field( 'ytt', [
	'type'        => 'color',
	'settings'    => 'button_border_hover',
	'label'       => __( 'Button Border Hover Color.', 'yourtechtherapist' ),
	'description' => esc_html__( 'Change button border hover color.', 'yourtechtherapist' ),
	'section'     => 'button_defaults',
    'active_callback' => [
        [
            'setting'  => 'button_hover_swap',
            'operator' => '==',
            'value'    => false,
        ]
    ],
	'default'     => '#349BB3',
    'output'      => [
        [
            'element' => ['button:hover', 'input[type="submit"]:hover', '.button:hover', '.wp-block-button a:hover', '.wp-block-uagb-cf7-styler:hover', '.uagb-cf7-styler__btn-align-center:hover', 'input.wpcf7-form-control.wpcf7-submit:hover', '.wpforms-form button:hover', 'button.wpforms-submit:hover'],
            'property' => 'border-color',
        ],
        [
            'element' => ['.edit-post-visual-editor.editor-styles-wrapper .block-editor-block-list__layout  .wp-block .wp-block-button .wp-block-button__link:hover', '.edit-post-visual-editor.editor-styles-wrapper .block-editor-block-list__layout  .wp-block .button:hover'],
            'property' => 'border-color',
            'context' => ['editor'],
        ],
    ],
	'choices'     => [
		'alpha' => true,
	],
] );

// Forms Section
Kirki::add_section( 'form_defaults', array(
    'title'          => esc_html__( 'Forms', 'yourtechtherapist' ),
    'description'    => esc_html__( 'Default layout and colors for all form fields.', 'yourtechtherapist' ),
    'panel'          => 'content_panel',
    'priority'       => 70,
) );

// Form Field Padding
Kirki::add_field( 'ytt', [
	'type'        => 'dimensions',
	'settings'    => 'form_padding',
	'label'       => esc_html__( 'Form Field Padding', 'yourtechtherapist' ),
	'section'     => 'form_defaults',
	'default'     => [
		'padding-top'    => '0.75rem',
		'padding-bottom' => '0.75rem',
		'padding-left'   => '0.75rem',
		'padding-right'  => '0.75rem',
    ],
	'choices'     => [
		'labels' => [
			'padding-top' => esc_html__( 'Top', 'yourtechtherapist' ),
			'padding-bottom'  => esc_html__( 'Bottom', 'yourtechtherapist' ),
			'padding-left' => esc_html__( 'Left', 'yourtechtherapist' ),
			'padding-right' => esc_html__( 'Right', 'yourtechtherapist' ),
		],
	],
    'output'      => [
        [
            'element' => ['input', 'textarea', 'select', 'body div.wpforms-container .wpforms-form .choices__inner', '.select2-container--default .select2-selection--single .select2-selection__rendered'],
        ],
        [
            'element' => ['.edit-post-visual-editor.editor-styles-wrapper .block-editor-block-list__layout  .wp-block input', '.edit-post-visual-editor.editor-styles-wrapper .block-editor-block-list__layout  .wp-block textarea', '.edit-post-visual-editor.editor-styles-wrapper .block-editor-block-list__layout  .wp-block select', '.edit-post-visual-editor.editor-styles-wrapper .block-editor-block-list__layout body div.wpforms-container .wpforms-form .choices__inner'],
            'context' => ['editor'],
        ],
    ],
] );

// Form Field Border Radius
Kirki::add_field( 'ytt', [
	'type'        => 'slider',
	'settings'    => 'form_border_radius',
	'label'       => esc_html__( 'Form Field Border Radius', 'yourtechtherapist' ),
	'section'     => 'form_defaults',
	'default'     => '0.25',
	'choices'     => [
		'min'  => 0,
		'max'  => 5,
		'step' => 0.05,
	],
    'output'      => [
        [
            'element' => ['input', 'textarea', 'select', '.wpforms-form input', '.wpforms-form textarea', '.wpforms-form select', 'body div.wpforms-container .wpforms-form .choices__inner', '.select2-container--default .select2-selection--single'],
            'property' => 'border-radius',
            'units' => 'em',
        ],
        [
            'element' => ['.edit-post-visual-editor.editor-styles-wrapper .block-editor-block-list__layout  .wp-block input', '.edit-post-visual-editor.editor-styles-wrapper .block-editor-block-list__layout  .wp-block textarea', '.edit-post-visual-editor.editor-styles-wrapper .block-editor-block-list__layout  .wp-block select', '.edit-post-visual-editor.editor-styles-wrapper .block-editor-block-list__layout body div.wpforms-container .wpforms-form .choices__inner'],
            'property' => 'border-radius',
            'units' => 'em',
            'context' => ['editor'],
        ],
    ],
] );

// Form Field Text Color
Kirki::add_field( 'ytt', [
	'type'        => 'color',
	'settings'    => 'form_text',
	'label'       => __( 'Form Field Text Color.', 'yourtechtherapist' ),
	'description' => esc_html__( 'Change form field text color.', 'yourtechtherapist' ),
	'section'     => 'form_defaults',
	'default'     => '#333333',
    'output'      => [
        [
            'element' => ['input', '.wpforms-form input', '.wpforms-form textarea', '.wpforms-form select', 'input[type="checkbox"]:checked:after', 'input[type="radio"]:checked:after', 'textarea', 'select', '.wpforms-form input[type="checkbox"]', '.wpforms-form input[type="radio"]', 'body div.wpforms-container .wpforms-form .choices__inner', '.select2-container--default .select2-selection--single'],
            'property' => 'color',
        ],
        [
            'element' => ['.edit-post-visual-editor.editor-styles-wrapper .block-editor-block-list__layout  .wp-block input', '.edit-post-visual-editor.editor-styles-wrapper .block-editor-block-list__layout  .wp-block textarea', '.edit-post-visual-editor.editor-styles-wrapper .block-editor-block-list__layout  .wp-block select', '.edit-post-visual-editor.editor-styles-wrapper .block-editor-block-list__layout  .wp-block input[type="checkbox"]:checked:after', '.edit-post-visual-editor.editor-styles-wrapper .block-editor-block-list__layout  .wp-block input[type="radio"]:checked:after', '.edit-post-visual-editor.editor-styles-wrapper .block-editor-block-list__layout  body div.wpforms-container .wpforms-form .choices__inner'],
            'property' => 'color',
            'context' => ['editor'],
        ],
    ],
] );

// Form Field Placeholder Color
Kirki::add_field( 'ytt', [
	'type'        => 'color',
	'settings'    => 'form_placeholder',
	'label'       => __( 'Form Field Placeholder Color.', 'yourtechtherapist' ),
	'description' => esc_html__( 'Change form field placeholder color.', 'yourtechtherapist' ),
	'section'     => 'form_defaults',
	'default'     => '#333333',
    'output'      => [
        [
            'element' => '::placeholder',
            'property' => 'color',
        ],
        [
            'element' => '::-webkit-input-placeholder',
            'property' => 'color',
        ],
        [
            'element' => '::-moz-placeholder',
            'property' => 'color',
        ],
        [
            'element' => ':-ms-input-placeholder',
            'property' => 'color',
        ],
        [
            'element' => ':moz-placeholder',
            'property' => 'color',
        ],
        [
            'element' => 'body div.wpforms-container .wpforms-form .choices__inner .choices__placeholder',
            'property' => 'color'
        ],
        [
            'element' => '.edit-post-visual-editor.editor-styles-wrapper .block-editor-block-list__layout  .wp-block *::placeholder',
            'property' => 'color',
            'context' => ['editor'],
        ],
        [
            'element' => '.edit-post-visual-editor.editor-styles-wrapper .block-editor-block-list__layout  .wp-block *::-webkit-input-placeholder',
            'property' => 'color',
            'context' => ['editor'],
        ],
        [
            'element' => '.edit-post-visual-editor.editor-styles-wrapper .block-editor-block-list__layout  .wp-block *::-moz-placeholder',
            'property' => 'color',
            'context' => ['editor'],
        ],
        [
            'element' => '.edit-post-visual-editor.editor-styles-wrapper .block-editor-block-list__layout  .wp-block *:-ms-input-placeholder',
            'property' => 'color',
            'context' => ['editor'],
        ],
        [
            'element' => '.edit-post-visual-editor.editor-styles-wrapper .block-editor-block-list__layout  .wp-block *:moz-placeholder',
            'property' => 'color',
            'context' => ['editor'],
        ],
        [
            'element' => '.edit-post-visual-editor.editor-styles-wrapper .block-editor-block-list__layout  body div.wpforms-container .wpforms-form .choices__inner .choices__placeholder',
            'property' => 'color',
            'context' => ['editor'],
        ]
    ],
] );

// Form Field Background Color
Kirki::add_field( 'ytt', [
	'type'        => 'color',
	'settings'    => 'form_background',
	'label'       => __( 'Form Field Background Color.', 'yourtechtherapist' ),
	'description' => esc_html__( 'Change form field background color.', 'yourtechtherapist' ),
	'section'     => 'form_defaults',
	'default'     => '#FFFFFF',
    'output'      => [
        [
            'element' => ['input', 'textarea', 'select', '.wpforms-form input', '.wpforms-form textarea', '.wpforms-form select', 'body div.wpforms-container .wpforms-form .choices__inner', '.select2-container--default .select2-selection--single'],
            'property' => 'background-color',
        ],
        [
            'element' => ['.edit-post-visual-editor.editor-styles-wrapper .block-editor-block-list__layout  .wp-block input', '.edit-post-visual-editor.editor-styles-wrapper .block-editor-block-list__layout  .wp-block textarea', '.edit-post-visual-editor.editor-styles-wrapper .block-editor-block-list__layout  .wp-block select', '.edit-post-visual-editor.editor-styles-wrapper .block-editor-block-list__layout body div.wpforms-container .wpforms-form .choices__inner'],
            'property' => 'background-color',
            'context' => ['editor'],
        ],
    ],
] );

// Form Border Color
Kirki::add_field( 'ytt', [
	'type'        => 'color',
	'settings'    => 'form_border_color',
	'label'       => __( 'Form Field Border Color.', 'yourtechtherapist' ),
	'description' => esc_html__( 'Change border of form fields.', 'yourtechtherapist' ),
	'section'     => 'form_defaults',
	'default'     => '#349BB3',
    'output'      => [
        [
            'element' => ['input', 'textarea', 'select', '.wpforms-form input', '.wpforms-form textarea', '.wpforms-form select', 'body div.wpforms-container .wpforms-form .choices__inner', '.select2-container--default .select2-selection--single'],
            'property' => 'border-color',
        ],
        [
            'element' => ['.edit-post-visual-editor.editor-styles-wrapper .block-editor-block-list__layout  .wp-block input', '.edit-post-visual-editor.editor-styles-wrapper .block-editor-block-list__layout  .wp-block textarea', '.edit-post-visual-editor.editor-styles-wrapper .block-editor-block-list__layout .wp-block select', '.edit-post-visual-editor.editor-styles-wrapper .block-editor-block-list__layout body div.wpforms-container .wpforms-form .choices__inner'],
            'property' => 'border-color',
            'context' => ['editor'],
        ],
    ],
	'choices'     => [
		'alpha' => true,
	],
] );

// Form Border Focus Color
Kirki::add_field( 'ytt', [
	'type'        => 'color',
	'settings'    => 'form_focus_color',
	'label'       => __( 'Form Field Focus Color.', 'yourtechtherapist' ),
	'description' => esc_html__( 'Change border of form fields when in focus.', 'yourtechtherapist' ),
	'section'     => 'form_defaults',
	'default'     => '#349BB3',
    'output'      => [
        [
            'element' => ['input:focus', 'textarea:focus', 'select:focus', 'input[type="checkbox"]:checked', 'input[type="radio"]:checked', '.wpforms-form input:focus', '.wpforms-form textarea:focus', '.wpforms-form select:focus', 'body div.wpforms-container .wpforms-form .is-focused .choices__inner, div.wpforms-container .wpforms-form .is-open .choices__inner', '.select2-container--default.select2-container--open.select2-container--below .select2-selection--single'],
            'property' => 'border-color',
        ],
        [
            'element' => ['.edit-post-visual-editor.editor-styles-wrapper .block-editor-block-list__layout  .wp-block input:focus', '.edit-post-visual-editor.editor-styles-wrapper .block-editor-block-list__layout  .wp-block textarea:focus', '.edit-post-visual-editor.editor-styles-wrapper .block-editor-block-list__layout  .wp-block select:focus', '.edit-post-visual-editor.editor-styles-wrapper .block-editor-block-list__layout  .wp-block input[type="checkbox"]:checked', '.edit-post-visual-editor.editor-styles-wrapper .block-editor-block-list__layout  .wp-block input[type="radio"]:checked', '.edit-post-visual-editor.editor-styles-wrapper .block-editor-block-list__layout div.wpforms-container .wpforms-form .is-focused .choices__inner, div.wpforms-container .wpforms-form .is-open .choices__inner'],
            'property' => 'border-color',
            'context' => ['editor'],
        ],
    ],
	'choices'     => [
		'alpha' => true,
	],
] );

// Search Forms Section
Kirki::add_section( 'search_form_defaults', array(
    'title'          => esc_html__( 'Search Forms', 'yourtechtherapist' ),
    'description'    => esc_html__( 'Specific settings for the search forms input and button.', 'yourtechtherapist' ),
    'panel'          => 'content_panel',
    'priority'       => 70,
) );

// Search Form Field Padding
Kirki::add_field( 'ytt', [
	'type'        => 'dimensions',
	'settings'    => 'search_form_padding',
	'label'       => esc_html__( 'Search Form Field and Button Padding', 'yourtechtherapist' ),
	'section'     => 'search_form_defaults',
	'default'     => [
		'padding-top'    => '0.5rem',
		'padding-bottom' => '0.5rem',
		'padding-left'   => '0.5rem',
		'padding-right'  => '0.5rem',
    ],
	'choices'     => [
		'labels' => [
			'padding-top' => esc_html__( 'Top', 'yourtechtherapist' ),
			'padding-bottom'  => esc_html__( 'Bottom', 'yourtechtherapist' ),
			'padding-left' => esc_html__( 'Left', 'yourtechtherapist' ),
			'padding-right' => esc_html__( 'Right', 'yourtechtherapist' ),
		],
	],
    'output'      => [
        'element' => ['.searchform input', '.searchform button'],
    ],
] );

// Search Form Field Border Radius
Kirki::add_field( 'ytt', [
	'type'        => 'slider',
	'settings'    => 'search_form_border_radius',
	'label'       => esc_html__( 'Search Form Field and Button Border Radius', 'yourtechtherapist' ),
	'section'     => 'search_form_defaults',
	'default'     => '0.25',
	'choices'     => [
		'min'  => 0,
		'max'  => 5,
		'step' => 0.05,
	],
    'output'      => [
        'element' => ['.searchform input', '.searchform button'],
        'property' => 'border-radius',
        'units' => 'em',
    ],
] );

// Search Form Field Font Size
Kirki::add_field( 'ytt', [
	'type'        => 'dimension',
	'settings'    => 'search_form_field_font_size',
	'label'       => esc_html__( 'Search Form Field Font Size', 'yourtechtherapist' ),
	'section'     => 'search_form_defaults',
	'default'     => '0.85rem',
    'output'      => [
        'element' => '.searchform input',
        'property' => 'font-size',
    ],
] );

// Search Form Button Font Size
Kirki::add_field( 'ytt', [
	'type'        => 'dimension',
	'settings'    => 'search_form_button_font_size',
	'label'       => esc_html__( 'Search Form Button Font Size', 'yourtechtherapist' ),
	'section'     => 'search_form_defaults',
	'default'     => '0.95rem',
    'output'      => [
        'element' => '.searchform button',
        'property' => 'font-size',
    ],
] );