<?php

/***************************
//// 

CUSTOMIZER - FOOTER

Footer Settings in the WordPress Customizer.

////
***************************/


/************************
//// FOOTER CONTROLS ////
************************/

// Kirki
    
/*********************
//// FOOTER PANEL ////
*********************/
Kirki::add_panel( 'footer_panel', array(
    'priority'    => 160,
    'title'       => esc_html__( 'Footer', 'yourtechtherapist' ),
    'description' => esc_html__( 'Adjust your Footer settings.', 'yourtechtherapist' ),
) );

// Footer Layout Section
Kirki::add_section( 'primary_footer', array(
    'title'          => esc_html__( 'Primary Footer Layout', 'yourtechtherapist' ),
    'description'    => esc_html__( 'Adjust footer layout.', 'yourtechtherapist' ),
    'panel'          => 'footer_panel',
    'priority'       => 70,
) );

// Primary Footer Padding
Kirki::add_field( 'ytt', [
	'type'        => 'dimensions',
	'settings'    => 'primary_footer_padding',
	'label'       => esc_html__( 'Primary Footer Padding', 'yourtechtherapist' ),
	'description' => esc_html__( 'Add padding to the primary footer. Must be a valid CSS unit.', 'yourtechtherapist' ),
	'section'     => 'primary_footer',
	'default'     => [
		'padding-top'  => '1rem',
		'padding-bottom'  => '1rem',
        'padding-left'  => '1rem',
        'padding-right'  => '1rem',
	],
    'choices'     => [
		'labels'  => [
			'padding-top'  => esc_html__( 'Top', 'yourtechtherapist' ),
			'padding-bottom'  => esc_html__( 'Bottom', 'yourtechtherapist' ),
			'padding-left' => esc_html__( 'Left', 'yourtechtherapist' ),
			'padding-right' => esc_html__( 'Right', 'yourtechtherapist' ),
		],
	],
    'output'      => [
        'element'  => 'footer.primary',
    ],
] );

// Footer Content Alignment
Kirki::add_field( 'ytt', [
	'type'        => 'select',
	'settings'    => 'primary_footer_content_align',
	'label'       => esc_html__( 'Text Alignment', 'yourtechtherapist' ),
	'description' => esc_html__( 'Text alignent for the primary footer.', 'yourtechtherapist' ),
	'section'     => 'primary_footer',
	'default'     => 'center',
	'placeholder' => esc_html__( 'Select an option...', 'yourtechtherapist' ),
	'priority'    => 10,
	'multiple'    => 1,
	'choices'     => [
		'center' => esc_html__( 'Center', 'yourtechtherapist' ),
		'left' => esc_html__( 'Left', 'yourtechtherapist' ),
		'right' => esc_html__( 'Right', 'yourtechtherapist' ),
	],
    'output'      => [
        'element'  => ['footer.primary', '.wrapper', 'footer.primary', '.wrapper h1', 'footer.primary .wrapper h2', 'footer.primary .wrapper h3', 'footer.primary .wrapper h4', 'footer.primary .wrapper h5', 'footer.primary .wrapper h6'],
        'property' => 'text-align',
    ],
] );

// Primary Footer Container Width
Kirki::add_field( 'ytt', [
	'type'        => 'dimension',
	'settings'    => 'footer_width',
	'label'       => esc_html__( 'Footer Container Width', 'yourtechtherapist' ),
    'description'    => esc_html__( 'Adjust the width of the footer container.', 'yourtechtherapist' ),
	'section'     => 'primary_footer',
	'default'     => '100%',
	'priority'    => 10,
    'output'      => [
        'element' => 'footer.primary',
        'property' => 'max-width',
    ],
] );

// Primary Footer Content Width
Kirki::add_field( 'ytt', [
	'type'        => 'dimension',
	'settings'    => 'footer_content_width',
	'label'       => esc_html__( 'Footer Content Width', 'yourtechtherapist' ),
    'description'    => esc_html__( 'Adjust the width of the footer content.', 'yourtechtherapist' ),
	'section'     => 'primary_footer',
	'default'     => '100%',
	'priority'    => 10,
    'output'      => [
        'element' => 'footer.primary .wrapper',
        'property' => 'max-width',
    ],
] );

// Primary Footer Content layout
Kirki::add_field( 'ytt', [
	'type'        => 'select',
	'settings'    => 'footer_content_layout',
	'label'       => esc_html__( 'Content Layout', 'yourtechtherapist' ),
    'description' => esc_html__( 'Adjust the layout of the footer.' ),
	'section'     => 'primary_footer',
	'default'     => 'layout_horizontal',
	'placeholder' => esc_html__( 'Select an option...', 'yourtechtherapist' ),
	'priority'    => 10,
	'multiple'    => 1,
	'choices'     => [
		'layout_horizontal' => esc_html__( 'Horizontal Layout', 'yourtechtherapist' ),
		'layout_stacked' => esc_html__( 'Stacked Layout', 'yourtechtherapist' ),
		'layout_reversed' => esc_html__( 'Reversed Horizontal', 'yourtechtherapist' ),
	],
] );

// Primary Footer Text Align
Kirki::add_field( 'ytt', [
	'type'        => 'select',
	'settings'    => 'footer_text_align',
	'label'       => esc_html__( 'Content Alignment', 'yourtechtherapist' ),
    'description' => esc_html__( 'Align the primary footer content left, right, or center.', 'yourtechtherapist' ),
	'section'     => 'primary_footer',
	'default'     => 'center',
	'placeholder' => esc_html__( 'Select an option...', 'yourtechtherapist' ),
	'priority'    => 10,
	'multiple'    => 1,
	'choices'     => [
		'center' => esc_html__( 'Center', 'yourtechtherapist' ),
		'left' => esc_html__( 'Left', 'yourtechtherapist' ),
		'right' => esc_html__( 'Right', 'yourtechtherapist' ),
	],
    'output'      => [
        'element' => 'footer.primary .wrapper',
        'property' => 'text-align',
    ]
] );

    
/********************************
//// SECONDARY FOOTER LAYOUT ////
********************************/

// Secondary Footer Layout
Kirki::add_section( 'secondary_footer', array(
    'title'          => esc_html__( 'Secondary Footer Layout', 'yourtechtherapist' ),
    'description'    => esc_html__( 'Adjust secondary footer layout.', 'yourtechtherapist' ),
    'panel'          => 'footer_panel',
    'priority'       => 70,
) );

// Secondary Footer Padding
Kirki::add_field( 'ytt', [
	'type'        => 'dimensions',
	'settings'    => 'secondary_footer_padding',
	'label'       => esc_html__( 'Secondary Footer Padding', 'yourtechtherapist' ),
	'description' => esc_html__( 'Add padding to the secondary footer.', 'yourtechtherapist' ),
	'section'     => 'secondary_footer',
	'default'     => [
		'padding-top'  => '1rem',
		'padding-bottom'  => '1rem',
        'padding-left'  => '1rem',
        'padding-right'  => '1rem',
	],
    'choices'     => [
		'labels'  => [
			'padding-top'  => esc_html__( 'Top', 'yourtechtherapist' ),
			'padding-bottom'  => esc_html__( 'Bottom', 'yourtechtherapist' ),
			'padding-left' => esc_html__( 'Left', 'yourtechtherapist' ),
			'padding-right' => esc_html__( 'Right', 'yourtechtherapist' ),
		],
	],
    'output'      => [
        'element'  => 'footer.secondary',
    ],
] );

// Show Header Menu In Footer
Kirki::add_field( 'ytt', [
	'type'        => 'toggle',
	'settings'    => 'menu_in_footer',
	'label'       => esc_html__( 'Show Menu In Footer', 'yourtechtherapist' ),
	'description' => esc_html__( 'Show the footer menu in the secondary footer.', 'yourtechtherapist' ),
	'section'     => 'secondary_footer',
	'default'     => 1,
] );

// Secondary Footer Layout > Footer Copyright
Kirki::add_field( 'ytt', [
    'type'        => 'textarea',
    'settings'    => 'footer_copyright',
    'label'       => esc_html__( 'Copyright Text', 'yourtechtherapist' ),
    'description' => esc_html__( 'Change the copyright text in the secondary footer. Accepts shortcodes', 'yourtechtherapist' ),
    'section'     => 'secondary_footer',
    'default'     => '© Copyright [site-name] [current-year]',
    'priority'    => 10,
]);


/*********************
//// FOOTER COLOR ////
*********************/

// Footer Color Section
Kirki::add_section( 'footer_color', array(
    'title'          => esc_html__( 'Footer Colors', 'yourtechtherapist' ),
    'panel'          => 'footer_panel',
    'priority'       => 70,
) );

// Footer Background Color
Kirki::add_field( 'ytt', [
	'type'        => 'color',
	'settings'    => 'footer_background',
	'label'       => __( 'Primary Footer Background Color', 'yourtechtherapist' ),
	'description' => esc_html__( 'Change the primary footer background color.', 'yourtechtherapist' ),
	'section'     => 'footer_color',
	'default'     => '#349BB3',
    'output'      => [
        'element' => 'footer.primary',
        'property' => 'background-color',
    ],
] );

// Footer Text Color
Kirki::add_field( 'ytt', [
	'type'        => 'color',
	'settings'    => 'footer_text',
	'label'       => __( 'Primary Footer Text Color', 'yourtechtherapist' ),
	'description' => esc_html__( 'Change the primary footer text color.', 'yourtechtherapist' ),
	'section'     => 'footer_color',
	'default'     => '#FFFFFF',
    'output'      => [
        'element' => ['footer.primary', 'footer.primary h1', 'footer.primary h2', 'footer.primary h3', 'footer.primary h4', 'footer.primary h5', 'footer.primary h6'],
        'property' => 'color',
    ],
] );

// Secondary Footer Background Color
Kirki::add_field( 'ytt', [
	'type'        => 'color',
	'settings'    => 'secondary_footer_background',
	'label'       => __( 'Secondary Footer Background Color', 'yourtechtherapist' ),
	'description' => esc_html__( 'Change the secondary footer background color.', 'yourtechtherapist' ),
	'section'     => 'footer_color',
	'default'     => '#333333',
    'output'      => [
        'element' => 'footer.secondary',
        'property' => 'background-color',
    ],
] );

// Secondary Footer Text Color
Kirki::add_field( 'ytt', [
	'type'        => 'color',
	'settings'    => 'secondary_footer_text',
	'label'       => __( 'Secondary Footer Text Color', 'yourtechtherapist' ),
	'description' => esc_html__( 'Change the secondary footer text color.', 'yourtechtherapist' ),
	'section'     => 'footer_color',
	'default'     => '#FFFFFF',
    'output'      => [
        'element' => 'footer.secondary',
        'property' => 'color',
    ],
] );

// Footer Link Color
Kirki::add_field( 'ytt', [
    'type'        => 'multicolor',
    'settings'    => 'footer_link_colors',
    'label'       => esc_html__( 'Link Colors', 'yourtechtherapist' ),
	'description' => esc_html__( 'Change text color for all links in the footer.', 'yourtechtherapist' ),
    'section'     => 'footer_color',
    'priority'    => 10,
    'choices'     => [
        'default'    => esc_html__( 'Normal', 'kirki' ),
        'hover'   => esc_html__( 'Hover', 'kirki' ),
        'visited'  => esc_html__( 'Visited', 'kirki' ),
    ],
    'default'     => [
        'default'    => '#349BB3',
        'hover'   => '#74E6FF',
        'visited'  => '#74E6FF',
    ],
    'output'      => [
        [
            'choice' => 'default',
            'element' => ['footer#primaryfooter a:not(.button)', 'footer#secondaryfooter a:not(.button)'],
            'property' => 'color',
        ],
        [
            'choice' => 'visited',
            'element' => ['footer#primaryfooter a:not(.button):visited', 'footer#secondaryfooter a:not(.button):visited'],
            'property' => 'color',
        ],
        [
            'choice' => 'hover',
            'element' => ['footer#primaryfooter a:not(.button):hover', 'footer#secondaryfooter a:not(.button):hover', 'footer#primaryfooter a:not(.button):hover .has-text-color', 'footer#secondaryfooter a:not(.button):hover .has-text-color'],
            'property' => 'color',1
        ],
    ],
] );