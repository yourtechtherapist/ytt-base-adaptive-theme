<?php

/***************************
//// 

CUSTOMIZER - COLORS

Color Settings in the WordPress Customizer.

////
***************************/


 
    
/**********************
//// COLOR PALETTE ////
**********************/

// Kirki

// Color Palette Section
Kirki::add_section( 'color_palette', array(
    'title'          => esc_html__( 'Color Palette', 'yourtechtherapist' ),
    'description'    => esc_html__( 'Gutenberg editor colorpalette', 'yourtechtherapist' ),
    'priority'       => 70,
) );

// Color One
Kirki::add_field( 'ytt', [
	'type'        => 'color',
	'settings'    => 'color_palette_one',
	'label'       => __( 'Color Palette One', 'yourtechtherapist' ),
	'section'     => 'color_palette',
	'default'     => '#349BB3',
    'output'      => [
        [
            'element' => ['.has-colorone-color', '.has-colorone-color:visited'],
            'property' => 'color',
        ],
        [
            'element' => ['header #nav .has-colorone-color a', 'header #nav .has-colorone-color a:visited'],
            'property' => 'color',
        ],
        [
            'element' => ['header #nav .has-colorone-color-hover a:hover'],
            'property' => 'color',
        ],
        [
            'element' => '.has-colorone-background-color',
            'property' => 'background-color',
        ],
        [
            'element' => ['.has-colorone-color', '.has-colorone-color:visited'],
            'property' => 'color',
            'context' => ['editor'],
        ],
        [
            'element' => '.has-colorone-background-color',
            'property' => 'background-color',
            'context' => ['editor'],
        ],
    ],
] );

// Color Two
Kirki::add_field( 'ytt', [
	'type'        => 'color',
	'settings'    => 'color_palette_two',
	'label'       => __( 'Color Palette Two', 'yourtechtherapist' ),
	'section'     => 'color_palette',
	'default'     => '#74E6FF',
    'output'      => [
        [
            'element' => ['.has-colortwo-color', '.has-colortwo-color:visited'],
            'property' => 'color',
        ],
        [
            'element' => ['header #nav .has-colortwo-color a', 'header #nav .has-colortwo-color a:visited'],
            'property' => 'color',
        ],
        [
            'element' => ['header #nav .has-colortwo-color-hover a:hover'],
            'property' => 'color',
        ],
        [
            'element' => '.has-colortwo-background-color',
            'property' => 'background-color',
        ],
        [
            'element' => ['.has-colortwo-color', '.has-colortwo-color:visited'],
            'property' => 'color',
            'context' => ['editor'],
        ],
        [
            'element' => '.has-colortwo-background-color',
            'property' => 'background-color',
            'context' => ['editor'],
        ],
    ],
] );

// Color Three
Kirki::add_field( 'ytt', [
	'type'        => 'color',
	'settings'    => 'color_palette_three',
	'label'       => __( 'Color Palette Three', 'yourtechtherapist' ),
	'section'     => 'color_palette',
	'default'     => '#FFC896',
    'output'      => [
        [
            'element' => ['.has-colorthree-color', '.has-colorthree-color:visited'],
            'property' => 'color',
        ],
        [
            'element' => ['header #nav .has-colorthree-color a', 'header #nav .has-colorthree-color a:visited'],
            'property' => 'color',
        ],
        [
            'element' => ['header #nav .has-colorthree-color-hover a:hover'],
            'property' => 'color',
        ],
        [
            'element' => '.has-colorthree-background-color',
            'property' => 'background-color',
        ],
        [
            'element' => ['.has-colorthree-color', '.has-colorthree-color:visited'],
            'property' => 'color',
            'context' => ['editor'],
        ],
        [
            'element' => '.has-colorthree-background-color',
            'property' => 'background-color',
            'context' => ['editor'],
        ],
    ],
] );

// Color Four
Kirki::add_field( 'ytt', [
	'type'        => 'color',
	'settings'    => 'color_palette_four',
	'label'       => __( 'Color Palette Four', 'yourtechtherapist' ),
	'section'     => 'color_palette',
	'default'     => '#B37946',
    'output'      => [
        [
            'element' => ['.has-colorfour-color', '.has-colorfour-color:visited'],
            'property' => 'color',
        ],
        [
            'element' => ['header #nav .has-colorfour-color a', 'header #nav .has-colorfour-color a:visited'],
            'property' => 'color',
        ],
        [
            'element' => ['header #nav .has-colorfour-color-hover a:hover'],
            'property' => 'color',
        ],
        [
            'element' => '.has-colorfour-background-color',
            'property' => 'background-color',
        ],
        [
            'element' => ['.has-colorfour-color', '.has-colorfour-color:visited'],
            'property' => 'color',
            'context' => ['editor'],
        ],
        [
            'element' => '.has-colorfour-background-color',
            'property' => 'background-color',
            'context' => ['editor'],
        ],
    ],
] );

// Color Five
Kirki::add_field( 'ytt', [
	'type'        => 'color',
	'settings'    => 'color_palette_five',
	'label'       => __( 'Color Palette Five', 'yourtechtherapist' ),
	'section'     => 'color_palette',
	'default'     => '#FFFFFF',
    'output'      => [
        [
            'element' => ['.has-colorfive-color', '.has-colorfive-color:visited'],
            'property' => 'color',
        ],
        [
            'element' => ['header #nav .has-colorfive-color a', 'header #nav .has-colorfive-color a:visited'],
            'property' => 'color',
        ],
        [
            'element' => ['header #nav .has-colorfive-color-hover a:hover'],
            'property' => 'color',
        ],
        [
            'element' => '.has-colorfive-background-color',
            'property' => 'background-color',
        ],
        [
            'element' => ['.has-colorfive-color', '.has-colorfive-color:visited'],
            'property' => 'color',
            'context' => ['editor'],
        ],
        [
            'element' => '.has-colorfive-background-color',
            'property' => 'background-color',
            'context' => ['editor'],
        ],
    ],
] );

// Color Six
Kirki::add_field( 'ytt', [
	'type'        => 'color',
	'settings'    => 'color_palette_six',
	'label'       => __( 'Color Palette Six', 'yourtechtherapist' ),
	'section'     => 'color_palette',
	'default'     => '#E1E1E1',
    'output'      => [
        [
            'element' => ['.has-colorsix-color', '.has-colorsix-color:visited'],
            'property' => 'color',
        ],
        [
            'element' => ['header #nav .has-colorsix-color a', 'header #nav .has-colorsix-color a:visited'],
            'property' => 'color',
        ],
        [
            'element' => ['header #nav .has-colorsix-color-hover a:hover'],
            'property' => 'color',
        ],
        [
            'element' => '.has-colorsix-background-color',
            'property' => 'background-color',
        ],
        [
            'element' => ['.has-colorsix-color', '.has-colorsix-color:visited'],
            'property' => 'color',
            'context' => ['editor'],
        ],
        [
            'element' => '.has-colorsix-background-color',
            'property' => 'background-color',
            'context' => ['editor'],
        ],
    ],
] );

// Accent Color
Kirki::add_field( 'ytt', [
	'type'        => 'color',
	'settings'    => 'accent_color',
	'label'       => __( 'Accent Color', 'yourtechtherapist' ),
	'description' => __( 'Define an accent color for minor elements such as separators, accordians, etc.', 'yourtechtherapist' ),
	'section'     => 'color_palette',
	'default'     => '#E1E1E1',
    'output'      => [
        [
            'element' => ['.accent-color-background', '.accent-color-background-after:after', '.accent-color-background-before:before', '.wp-block-getwid-toggle .wp-block-getwid-toggle__header-wrapper', '.wp-block-getwid-progress-bar__progress'],
            'property' => 'background',
        ],
        [
            'element' => ['.accent-color-text', '.accent-color-text a', '.accent-color-text-after:after', '.accent-color-text-before:before', '.wp-block-getwid-toggle .wp-block-getwid-toggle__header-wrapper', '.wp-block-getwid-progress-bar__progress', '.wp-block-quote p:before', '.wp-block-quote p:after'],
            'property' => 'color',
        ],
        [
            'element' => ['.wp-block-image figcaption', '.vcard img', '.is-style-bordered img'],
            'property' => 'border-color',
        ],
        [
            'element' => ['.edit-post-visual-editor.editor-styles-wrapper .wp-block-getwid-toggle .wp-block-getwid-toggle__header-wrapper', '.edit-post-visual-editor.editor-styles-wrapper .wp-block-image figcaption', '.edit-post-visual-editor.editor-styles-wrapper .wp-block-getwid-progress-bar__progress'],
            'property' => 'background-color',
            'context' => ['editor'],
        ],
        [
            'element' => ['.edit-post-visual-editor.editor-styles-wrapper .wp-block-image figcaption', '.edit-post-visual-editor.editor-styles-wrapper .is-style-bordered img'],
            'property' => 'border-color',
            'context' => ['editor'],
        ],
        [
            'element' => ['.edit-post-visual-editor.editor-styles-wrapper .wp-block-quote p:before', '.edit-post-visual-editor.editor-styles-wrapper .wp-block-quote p:after'],
            'property' => 'color',
            'context' => ['editor'],
        ],
    ],
] );


