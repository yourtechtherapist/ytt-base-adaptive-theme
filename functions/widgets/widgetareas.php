<?php

/***************************
//// 

WIDGET AREAS

Define the areas of the website that you want to allow widgets to work in.

////
***************************/

// Register widgetized areas
function theme_widgets_init() {

    // Page Sidebar
    register_sidebar( array (
        'name' => 'Page Sidebar',
        'id' => 'page_widget_area',
        'description' => 'Appears on all single pages.',
        'before_widget' => '<div class="widget">',
        'after_widget' => "</div>",
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
      ) );

    // Blog Sidebar
    register_sidebar( array (
        'name' => 'Blog Sidebar',
        'id' => 'blog_widget_area',
        'description' => 'Appears on all Blog index pages (Blog Index, Archive, Tags, Categories, Search, etc.).',
        'before_widget' => '<div class="widget">',
        'after_widget' => "</div>",
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '<span class="accent-color-background"></span></h3>',
      ) );
        
    // Footer 1
    register_sidebar( array (
        'name' => 'Footer One',
        'id' => 'footer_one_widget_area',
        'description' => 'Appears in the primary Footer.',
        'before_widget' => '<div class="widget">',
        'after_widget' => "</div>",
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '<span class="accent-color-background"></span></h3>',
      ) );
    
    // Footer 2
    register_sidebar( array (
        'name' => 'Footer Two',
        'id' => 'footer_two_widget_area',
        'description' => 'Appears in the primary Footer.',
        'before_widget' => '<div class="widget">',
        'after_widget' => "</div>",
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '<span class="accent-color-background"></span></h3>',
      ) );
    
    // Footer 3
    register_sidebar( array (
        'name' => 'Footer Three',
        'id' => 'footer_three_widget_area',
        'description' => 'Appears in the primary Footer.',
        'before_widget' => '<div class="widget">',
        'after_widget' => "</div>",
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '<span class="accent-color-background"></span></h3>',
      ) );

} // end theme_widgets_init
 
add_action( 'init', 'theme_widgets_init' );

$preset_widgets = array (
    'primary_widget_area'  => array( 'search', 'pages', 'categories', 'archives' )
);
if ( isset( $_GET['activated'] ) ) {
    update_option( 'sidebars_widgets', $preset_widgets );
}
// update_option( 'sidebars_widgets', NULL );


// Check for static widgets in widget-ready areas
function is_sidebar_active( $index ){
  global $wp_registered_sidebars;
 
  $widgetcolums = wp_get_sidebars_widgets();
 
  if ($widgetcolums[$index]) return true;
 
    return false;
} // end is_sidebar_active
