<?php

/***************************
//// 

CUSTOM SHORTCODES

Create custom shortcodes here.

////
***************************/

/*********************
//// CURRENT YEAR ////
*********************/
function current_year_shortcode() {
    ob_start();
    echo date('Y');
    return ob_get_clean();
}
add_shortcode('current-year', 'current_year_shortcode');


/******************
//// SITE NAME ////
******************/
function site_name_shortcode() {
    ob_start();
    echo bloginfo('name');
    return ob_get_clean();
}
add_shortcode('site-name', 'site_name_shortcode');