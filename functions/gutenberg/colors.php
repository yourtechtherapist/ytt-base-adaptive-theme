<?php

/***************************
//// 

CUSTOM GUTENBERG COLOR PALETTE

These settings control the custom Gutenberg color palette for your theme. You will also need to change the colors in the _variables.scss file to match.

////
***************************/


/********************************
//// GUTENBERG COLOR PALETTE ////
********************************/

// Set the color palette.
add_theme_support( 'editor-color-palette', array(
    array(
        'name'  => __( 'Color One', 'yourtechtherapist' ),
        'slug'  => 'colorone',
        'color'	=> esc_html( get_theme_mod( 'color_palette_one', '#349BB3' ) ),
    ),
    array(
        'name'  => __( 'Color Two', 'yourtechtherapist' ),
        'slug'  => 'colortwo',
        'color'	=> esc_html( get_theme_mod( 'color_palette_two', '#74E6FF' ) ),
    ),
    array(
        'name'  => __( 'Color Three', 'yourtechtherapist' ),
        'slug'  => 'colorthree',
        'color'	=> esc_html( get_theme_mod( 'color_palette_three', '#FFC896' ) ),
    ),
    array(
        'name'  => __( 'Color Four', 'yourtechtherapist' ),
        'slug'  => 'colorfour',
        'color'	=> esc_html( get_theme_mod( 'color_palette_four', '#B37946' ) ),
    ),
    array(
        'name'  => __( 'Color Five', 'yourtechtherapist' ),
        'slug'  => 'colorfive',
        'color'	=> esc_html( get_theme_mod( 'color_palette_five', '#FFFFFF' ) ),
    ),
    array(
        'name'  => __( 'Color Six', 'yourtechtherapist' ),
        'slug'  => 'colorsix',
        'color'	=> esc_html( get_theme_mod( 'color_palette_six', '#E1E1E1' ) ),
    ),
) );


/**************************
//// ACF COLOR PALETTE ////
**************************/

// Synchronizes ACF color picker with the Gutenberg custom color palette.
function wd_acf_color_palette() { ?>
<script type='text/javascript'>
(function($) {
     acf.add_filter('color_picker_args', function( args, $field ){
          // add the hexadecimal codes here for the colors you want to appear as swatches
          args.palettes = ['#349BB3', '#74E6FF', '#FFBA7D', '#FFC896', '#B37946', '#FFFFFF', '#E1E1E1', '#333333']
          // return colors
          return args;
     });
})(jQuery);
</script>
<?php }