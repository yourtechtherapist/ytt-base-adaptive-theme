<?php

/***************************
//// 

CUSTOM GUTENBERG BLOCKS

This is where all custom Gutenberg blocks are stored. These are created through Advanced Custom Fields, and that plugin will be required for these to functions.

////
***************************/


// Custom Gutenberg Blocks
add_action('acf/init', 'my_acf_init');
function my_acf_init() {
	
	// check function exists
	if( function_exists('acf_register_block') ) {
		
		// register Page Header block
		acf_register_block(array(
			'name'				=> 'pageheader',
			'title'				=> __('Page Header'),
			'description'		=> __('A custom page header'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'formatting',
			'icon'				=> 'align-center',
		));
	}
    
    // check function exists
	if( function_exists('acf_register_block') ) {
		
		// register Service block
		acf_register_block(array(
			'name'				=> 'service',
			'title'				=> __('Service'),
			'description'		=> __('Add a service'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'widgets',
			'icon'				=> 'shield-alt',
		));
	}
    
    // check function exists
	if( function_exists('acf_register_block') ) {
		
		// register Portfolio block
		acf_register_block(array(
			'name'				=> 'portfolio',
			'title'				=> __('Portfolio'),
			'description'		=> __('Add a portfolio item'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'widgets',
			'icon'				=> 'format-image',
		));
	}
    
    // check function exists
	if( function_exists('acf_register_block') ) {
		
		// register Portfolio block
		acf_register_block(array(
			'name'				=> 'ellipsis',
			'title'				=> __('Ellipsis'),
			'description'		=> __('Add an ellipsis'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'formatting',
			'icon'				=> 'editor-insertmore',
		));
	}
    
    // check function exists
	if( function_exists('acf_register_block') ) {
		
		// register Portfolio block
		acf_register_block(array(
			'name'				=> 'external-resource',
			'title'				=> __('External Resource'),
			'description'		=> __('Link to an external resource'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'widget',
			'icon'				=> 'admin-site-alt3',
		));
	}
    
    // check function exists
	if( function_exists('acf_register_block') ) {
		
		// Register News List block
        acf_register_block(array(
            'name'				=> 'postlist',
            'title'				=> __('Post List', 'yourtechtherapist'),
            'description'		=> __('Display recent posts', 'yourtechtherapist'),
            'render_callback'	=> 'my_acf_block_render_callback',
            'category'			=> 'widget',
            'icon'				=> 'align-center',
            'keywords'		    => array( 'post', 'list' ),
        ));
	}
}

// Render callback
function my_acf_block_render_callback( $block ) {
	
	// convert name ("acf/testimonial") into path friendly slug ("testimonial")
	$slug = str_replace('acf/', '', $block['name']);
	
	// include a template part from within the "template-parts/block" folder
	if( file_exists( get_theme_file_path("/template-parts/block/content-{$slug}.php") ) ) {
		include( get_theme_file_path("/template-parts/block/content-{$slug}.php") );
	}
}