<?php

/***************************
//// 

BLOG - INDEX

Settings for the blog index pages, including main index, tag, category, archive, and search.

////
***************************/
    
    
/************************
//// PAGE NAVIGATION ////
************************/


// Show page navigation only if pages exist
function show_posts_nav() {
	global $wp_query;
	return ($wp_query->max_num_pages > 1);
}

// Add .button class to navigation links
add_filter('next_posts_link_attributes', 'posts_link_attributes');
add_filter('previous_posts_link_attributes', 'posts_link_attributes');

function posts_link_attributes() {
  return 'class="button"';
}