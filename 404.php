<?php get_header(); ?>
 
        <div id="container" class="container404">
        
            <article id="content">
				
                <section id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    
                    <div class="wrapper">

                        <h1>404 Error</h1>

                        <h4>Oops! Looks like you reached a page that doesn't exist</h4>

                        <a class="button" ahref="<?php bloginfo('url'); ?>">Go home</a>

                    </div>
                    
                </section><!-- #post-<?php the_ID(); ?> -->                 
 
            </article><!-- #content -->
            
        </div><!-- #container -->
 
<?php get_footer(); ?>