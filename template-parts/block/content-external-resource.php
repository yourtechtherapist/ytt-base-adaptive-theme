<?php
/**
 * Block template file: 
 *
 * External Resource Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'external-resource-' . $block['id'];
if ( ! empty($block['anchor'] ) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$classes = 'block-external-resource';
if( ! empty( $block['className'] ) ) {
    $classes .= ' ' . $block['className'];
}
if( ! empty( $block['align'] ) ) {
    $classes .= ' align' . $block['align'];
}
?>

<style type="text/css">
	<?php echo '#' . $id; ?> {
		/* Add styles that use ACF values here */
	}
</style>

<div id="<?php echo esc_attr( $id ); ?>" class="<?php echo esc_attr( $classes ); ?>">
    <a href="<?php the_field( 'extres_link' ); ?>"><h4>
        <?php if ( get_field( 'extres_image' ) ) { ?>
            <img src="<?php the_field( 'extres_image' ); ?>" />
        <?php } ?>
        <?php the_field( 'extres_title' ); ?></h4>
    </a>
	<div><?php the_field( 'extres_description' ); ?></div>
</div>