<?php
/**
 * Block template file: 
 *
 * Pageheader Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'pageheader-' . $block['id'];
if ( ! empty($block['anchor'] ) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$classes = 'block-pageheader';
if( ! empty( $block['className'] ) ) {
    $classes .= ' ' . $block['className'];
}
if( ! empty( $block['align'] ) ) {
    $classes .= ' align' . $block['align'];
}
?>

<style type="text/css">
	<?php echo '#' . $id; ?> {
		/* Add styles that use ACF values here */
	}
</style>

<?php
if(get_field('background_image')) { ?>
    <div id="<?php echo esc_attr( $id ); ?>" class="bgimg <?php echo esc_attr( $classes ); ?>">
        <div class="wrapper">
            <h1><?php the_field( 'page_header' ); ?></h1>
            <h2 class="h2alt"><?php the_field( 'page_sub_header' ); ?></h2>
        </div>
    </div>
<?php } else { ?>
    <div id="<?php echo esc_attr( $id ); ?>" class="pageheader <?php echo esc_attr( $classes ); ?>">
        <div class="wrapper">
            <h1><?php the_field( 'page_header' ); ?></h1>
            <h2 class="h2alt"><?php the_field( 'page_sub_header' ); ?></h2>
        </div>
    </div>
<?php }
?>
<style type="text/css">
	#<?php echo $id; ?> {
        color: <?php the_field( 'text_color' ); ?>;
		background: <?php the_field( 'background_color' ); ?> url(<?php if ( get_field( 'background_image' ) ) { ?><?php the_field( 'background_image' ); ?><?php } ?>) no-repeat right center / auto 100%;
	}
</style>