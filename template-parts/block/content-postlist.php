<?php
/**
 * Block template file: 1
 *
 * Postlist Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'postlist-' . $block['id'];
if ( ! empty($block['anchor'] ) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$classes = 'block-postlist';
if( ! empty( $block['className'] ) ) {
    $classes .= ' ' . $block['className'];
}
if( ! empty( $block['align'] ) ) {
    $classes .= ' align' . $block['align'];
}

$postsPerPage = get_field( 'number_of_posts' );

$args = array( 
    'suppress_filters' => true,
    'post_type' => 'post',
    'posts_per_page' => $postsPerPage,
);

$textalign = get_field('posts_text_align');

?>

<div id="<?php echo esc_attr( $id ); ?>" class="<?php echo esc_attr( $classes ); ?> <?php echo $textalign; ?>">

    <?php $the_query = new WP_Query( $args );

    if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

        <div class="post">

            <h4 class="post-title is-style-alt"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>

            <div class="entry-meta post-meta">

                <span class="author name"><?php the_author(); ?></span>

                <span class="author meta-sep"> | </span>

                <span class="author date"><abbr class="published" title="<?php the_time('Y-m-d') ?>"><?php the_time( get_option( 'date_format' ) ); ?></abbr></span>
                
            </div>
            
            <div class="entry-content">
                <?php the_excerpt(); ?>

            <?php if ( comments_open() || get_comments_number() ) : ?>
                <span class="author comments">Comments (<?php echo get_comments_number(); ?>)</span>
            <?php endif; ?>
            </div><!-- .entry-content -->

            <div class="read-more">
                <a class="button moretag" href="<?php the_permalink(); ?>">More Info</a>
            </div>

        </div>

    <?php 
    endwhile;
    wp_reset_postdata();
    endif;
    ?>

</div>

<style type="text/css">
	<?php echo '#' . $id; ?> {
        display: -webkit-box;
        display: -ms-flexbox;
        display: -webkit-flex;
        display: flex;
        justify-content: flex-start;
        flex-direction: column;
        padding: 1.5rem 0 !important;
        flex-wrap: wrap;
	}
    
    <?php echo '#' . $id; ?>.textalign-center {
        text-align: center;
    }
    
    <?php echo '#' . $id; ?>.textalign-left {
        text-align: left;
    }
    
    <?php echo '#' . $id; ?>.textalign-right {
        text-align: right;
    }
    
    <?php echo '#' . $id; ?> .post {
        width: 100%;
        padding: 1rem;
        display: -webkit-box;
        display: -ms-flexbox;
        display: -webkit-flex;
        display: flex;
        justify-content: flex-start;
        flex-direction: column;
    }
    
    <?php echo '#' . $id; ?> .post .post-title {
        padding-top: 0;
        text-align: inherit;
    }
    
    <?php echo '#' . $id; ?> .post .post-title a {
        color: inherit;
    }
    
    <?php echo '#' . $id; ?> .post .entry-meta {
        font-size: 0.85em;
        font-style: italic;
    }
    
    <?php echo '#' . $id; ?> .post .entry-meta span {
        vertical-align: middle;
    }
    
    <?php echo '#' . $id; ?> .post .entry-meta span abbr {
        text-decoration: none;
    }
    
    <?php echo '#' . $id; ?> .post .entry-content {
        flex-grow: 1;
    }
    
    <?php echo '#' . $id; ?> .post .entry-content .comments {
        padding-bottom: 1rem;
        font-size: 0.85rem;
        font-style: italic;
        display: block;
    }
    
    <?php echo '#' . $id; ?> .post .read-more {
        text-align: center;
        margin-top: 0.5rem;
        margin-bottom: 0.5rem;
    }
    
    @media screen and (min-width: 768px) {
        <?php echo '#' . $id; ?> {
            flex-direction: row;
        }
        
        <?php echo '#' . $id; ?> .post {
            width: 50%;
        }
        
        <?php echo '#' . $id; ?> .post:first-child {
            width: 100%;
        }
    }
    
    @media screen and (min-width: 1024px) {
        <?php echo '#' . $id; ?> .post {
            width: 33.33%;
        }
        
        <?php echo '#' . $id; ?> .post:first-child {
            width: 33.33%;
        }
    }
</style>