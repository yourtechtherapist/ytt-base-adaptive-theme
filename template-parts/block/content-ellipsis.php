<?php
/**
 * Block template file: 
 *
 * Ellipsis Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'ellipsis-' . $block['id'];
if ( ! empty($block['anchor'] ) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$classes = 'block-ellipsis';
if( ! empty( $block['className'] ) ) {
    $classes .= ' ' . $block['className'];
}
if( ! empty( $block['align'] ) ) {
    $classes .= ' align' . $block['align'];
}
?>

<style type="text/css">
	<?php echo '#' . $id; ?> {
        color: <?php the_field( 'color' ); ?>;
	}
</style>

<div id="<?php echo esc_attr( $id ); ?>" class="<?php echo esc_attr( $classes ); ?> has-color-<?php echo $wd_color_class; ?>">
	<span>•••</span>
</div>

<?php
// Get ACF Values from color picker
$wd_acf_color_picker_values = get_field( 'color' );

// Set array of color classes (for block editor) and hex codes (from ACF)
$wd_block_colors = [
    // Change these to match your color class (gutenberg) and hex codes (acf)
    "mediumblue"   => "#349BB3",
    "lightblue"    => "#74E6FF",
    "mediumtan"    => "#FFBA7D",
    "lighttan"     => "#FFC896",
    "darktan"      => "#B37946",
    "white"        => "#FFFFFF",
    "light-gray"   => "#E1E1E1",
    "gray"         => "#333333",
];

// Loop over colors array and set proper class if background color selection matches value
foreach( $wd_block_colors as $key => $value ) {
     if( $wd_acf_color_picker_values == $value ) {
          $wd_color_class = $key;
     }
}