<?php
/**
 * Block template file: 
 *
 * Portfolio Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'portfolio-item-' . $block['id'];
if ( ! empty($block['anchor'] ) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$classes = 'block-portfolio-item';
if( ! empty( $block['className'] ) ) {
    $classes .= ' ' . $block['className'];
}
if( ! empty( $block['align'] ) ) {
    $classes .= ' align' . $block['align'];
}

$select_portfolio_item = get_field( 'select_portfolio_item' );
    $args = array( 
    'orderby' => 'title',
    'post_type' => 'portfolio',
    'p' => $select_portfolio_item
);

$the_query = new WP_Query( $args );

if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
  
<div class="<?php echo esc_attr( $classes ); ?>">
    
    <a class="item-link" href="<?php the_permalink(); ?>" title="<?php printf( __('Permalink to %s', 'yourtechtherapist'), the_title_attribute('echo=0') ); ?>" rel="bookmark">
    
        <?php

        if( have_rows('website_pages', get_the_ID() ) ):

        $start = 1; $end = 1;

            while ( have_rows('website_pages', get_the_ID() ) ) : the_row();

            if( $start <= $end ): ?>
                
                <div class="img-wrapper"><?php echo do_shortcode("[ss_screenshot width='400' site='".get_sub_field('page_url', get_the_ID() )."']"); ?></div>
                

           <?php  endif;

            $start++;

            endwhile;

        else :

            // no rows found

        endif;

        ?>

        <h4 class="title"><?php the_title(); ?></h4>
        
    </a>

</div>
  
<?php endwhile; ?>
<?php else: __( );  endif; ?>