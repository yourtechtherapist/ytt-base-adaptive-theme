<?php

/***************************
//// 

CUSTOMIZER VARIABLES - Content

Content variables to use as classes from the WordPress Customizer.

////
***************************/


$content_width = get_theme_mod( 'content_width' );

$content_content_width = get_theme_mod( 'content_content_width' );

?>