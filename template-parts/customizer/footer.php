<?php

/***************************
//// 

CUSTOMIZER VARIABLES - FOOTER

Footer variables to use as classes from the WordPress Customizer.

////
***************************/

$footer_content_layout = get_theme_mod( 'footer_content_layout', 'layout_horizontal' );

?>