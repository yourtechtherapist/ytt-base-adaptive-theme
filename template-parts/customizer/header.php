<?php

/***************************
//// 

CUSTOMIZER VARIABLES - HEADER

Header variables to use as classes from the WordPress Customizer.

////
***************************/


$header_location = get_theme_mod( 'header_location', 'location_top' );

if( (get_theme_mod( 'transparent_header', 0 ) == 1) || (get_field( 'transparent_header' ) == true) ) {
    $transparent_header = "transparent";
} else {
    $transparent_header = "not_transparent";
}

if(get_theme_mod( 'sticky_header', 0 ) == 1) {
    $sticky_header = "sticky_header";
} else {
    $sticky_header = "not_sticky";
}


$header_content_layout = get_theme_mod( 'header_content_layout', 'layout_horizontal' );
                    

if ( true == get_theme_mod( 'secondary_header_reverse_layout', false ) ) {
    $secondaryheaderlayout = 'reversed';
}
else {
    $secondaryheaderlayout = 'normal';
}


if ( true == get_theme_mod('announcement_bar_all_pages', true) ) {
    $announcementbarpages = 'all-pages';
}

?>