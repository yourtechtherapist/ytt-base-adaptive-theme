<ul class="share">

	<li><p>Share:</p></li>
	
</ul>

<ul class="share-icons">
	
	<li class="twitter"><a href="http://twitter.com/home?status=<?php the_title(); ?>+<?php the_permalink(); ?>" target="_blank">T</a></li>
	
	<li class="facebook"><a href="http://www.facebook.com/share.php?u=<?php the_permalink(); ?>&title=<?php the_title(); ?>" target="_blank">F</a></li>
	
	<li class="linkedin"><a href="http://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>&title=<?php the_title(); ?>&source=<?php get_home_url(); ?>" target="_blank">I</a></li>
	
	<li class="google"><a href="https://plus.google.com/share?url=<?php the_permalink(); ?>" target="_blank">g</a></li>
	
	<li class="tumblr"><a href="http://www.tumblr.com/share?v=3&u=<?php the_permalink(); ?>&t=<?php the_title(); ?>" target="_blank">O</a></li>
	
	<li class="stumbleupon"><a href="http://www.stumbleupon.com/submit?url=<?php the_permalink(); ?>&title=<?php the_title(); ?>" target="_blank">S</a></li>
	               
</ul><!-- .sharing -->

<div class="clearfix"></div>