<div id="sociallinks">
    <?php if ( have_rows( 'social_media_links', 'option' ) ) : ?>
        <?php while ( have_rows( 'social_media_links', 'option' ) ) : the_row(); ?>
            <a class="social link <?php the_sub_field( 'name' ); ?>" href="<?php the_sub_field( 'link' ); ?>">
                <?php the_sub_field( 'icon' ); ?>
            </a>
        <?php endwhile; ?>
    <?php else : ?>
        <?php // no rows found ?>
    <?php endif; ?>
</div>