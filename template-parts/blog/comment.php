<div id="div-comment-<?php comment_ID() ?>" class="comment-body">
    <div class="vcard">
        <?php
        // Display avatar unless size is set to 0
        if ( $args['avatar_size'] != 0 ) {
            $avatar_size = ! empty( $args['avatar_size'] ) ? $args['avatar_size'] : 100; // set default avatar size
                echo get_avatar( $comment, $avatar_size );
        } ?>
    </div><!-- .comment-author -->
    <div class="comment-details">
        <span class="comment-meta commentmetadata">

            <?php 
            // Display author name
            printf( __( '<cite class="fn">%s</cite>', 'textdomain' ), get_comment_author_link() ); ?>

            <a class="date" href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ); ?>"><?php
                /* translators: 1: date, 2: time */
                printf(
                    __( '%1$s', 'textdomain' ),
                    get_comment_date()
                ); ?>
            </a><?php
            edit_comment_link( __( '(Edit)', 'textdomain' ), '  ', '' ); ?>
        </span><!-- .comment-meta -->

        <div class="comment-text"><?php comment_text(); ?>
            <?php
            // Display comment moderation text
            if ( $comment->comment_approved == '0' ) { ?>
                <p class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.', 'textdomain' ); ?></p><br/><?php
            } ?>
        </div><!-- .comment-text -->

        <div class="reply">
            <?php
            // Display comment reply link
            comment_reply_link( array_merge( $args, array(
                'add_below' => $add_below,
                'depth'     => $depth,
                'max_depth' => $args['max_depth']
            ) ) ); ?>
        </div>
    </div><!-- .comment-details -->
</div>