<article id="post-<?php the_ID(); ?>" <?php post_class('loadmore'); ?>>

    <div class="featured-image">
        <a href="<?php the_permalink(); ?>" title="<?php printf( __('Permalink to %s', 'yourtechtherapist'), the_title_attribute('echo=0') ); ?>" rel="bookmark"><?php the_post_thumbnail(); ?></a>
    </div>

    <div class="content">

        <span class="category"><?php echo get_the_category_list(', '); ?></span>

        <h2 class="entry-title h2alt"><a href="<?php the_permalink(); ?>" title="<?php printf( __('Permalink to %s', 'yourtechtherapist'), the_title_attribute('echo=0') ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>

        <span class="date"><abbr class="published" title="<?php the_time('Y-m-d') ?>"><?php the_time( get_option( 'date_format' ) ); ?></abbr></span>

    </div>

</article><!-- article --> 