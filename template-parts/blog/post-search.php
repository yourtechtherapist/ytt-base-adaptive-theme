 <article id="post-<?php the_ID(); ?>" <?php post_class('post'); ?>>
     
    <div class="post-wrapper">
        
        <?php if (has_post_thumbnail()) {
            $thumbnail = 'thumbnail';
        } else {
            $thumbnail = 'no_thumbnail';
        } ?>
    
        <div class="featured-image <?php echo $thumbnail; ?>">

            <div class="entry-meta">
                        
                <span class="vcard">
                    <?php echo get_avatar( get_the_author_meta( 'ID' ), 50 ); ?>
                </span>

                <span class="author name"><?php the_author(); ?></span>

                <span class="author meta-sep"> | </span>

                <span class="author date"><abbr class="published" title="<?php the_time('Y-m-d') ?>"><?php the_time( get_option( 'date_format' ) ); ?></abbr></span>

            </div><!-- .entry-meta -->

            <?php the_post_thumbnail( 'index_featured' ); ?>

            <div class="post-type"><span class="accent-color-text"><?php echo ucfirst( get_post_type( get_the_ID() ) ); ?></span></div>

        </div>

         <div class="content-wrapper">

            <h2 class="entry-title is-style-alt"><a href="<?php the_permalink(); ?>" title="<?php printf( __('Permalink to %s', 'yourtechtherapist'), the_title_attribute('echo=0') ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>

            <div class="entry-content">
            <?php the_excerpt(); ?>
            <?php wp_link_pages('before=<div class="page-link">' . __( 'Pages:', 'yourtechtherapist' ) . '&after=</div>') ?>
            </div><!-- .entry-content -->
             
            <?php if ( comments_open() || get_comments_number() || has_category() ) : ?>
                <div class="entry-utility accent-color-text">
                    <?php if (has_category()) : ?>
                        <span class="cat-links"><?php echo get_the_category_list(', '); ?></span>
                    <?php endif; ?>
                    <?php if ( comments_open() || get_comments_number() ) : ?>
                        <span class="comments-link"><?php comments_popup_link( __( 'Leave a comment', 'yourtechtherapist' ), __( '1 Comment', 'yourtechtherapist' ), __( '% Comments', 'yourtechtherapist' ) ) ?></span>
                    <?php endif; ?>
                </div><!-- #entry-utility -->
            <?php endif; ?>

        </div><!-- .content-wrapper -->

        <div class="read-more">
            <a class="button moretag" href="<?php the_permalink(); ?>">Read More</a>
        </div>
    
    </div><!-- .post-wrapper -->

</article><!-- article -->     