<article id="post-<?php the_ID(); ?>" <?php post_class('loadmore'); ?>>

    <div class="featured-image">
        <a href="<?php the_permalink(); ?>" title="<?php printf( __('Permalink to %s', 'yourtechtherapist'), the_title_attribute('echo=0') ); ?>" rel="bookmark"><?php the_post_thumbnail( 'category_featured' ); ?></a>
    </div>

    <div class="post-content">
        <h2 class="entry-title h2alt"><a href="<?php the_permalink(); ?>" title="<?php printf( __('Permalink to %s', 'yourtechtherapist'), the_title_attribute('echo=0') ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>

        <span class="category"><?php echo get_the_category_list(', '); ?></span>

        <div class="entry-content">
        <?php the_excerpt(); ?>
        </div><!-- .entry-content -->

        <div class="entry-meta">

            <?php echo get_avatar( get_the_author_meta( 'ID' ), 50 ); ?>

            <span class="author name"><?php the_author(); ?></span>

            <span class="author meta-sep"> - </span>

            <span class="author date"><abbr class="published" title="<?php the_time('Y-m-d') ?>"><?php the_time( get_option( 'date_format' ) ); ?></abbr></span>

        </div><!-- .entry-meta -->
    </div>

</article><!-- article --> 