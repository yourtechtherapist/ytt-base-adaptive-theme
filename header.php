<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html <?php language_attributes(); ?> class="no-js">
<head>
 
    <meta http-equiv="content-type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
	
	<?php
	//loads comment reply JS on single posts and pages
	if ( is_single() || is_page() ) wp_enqueue_script( 'comment-reply' ); ?>
 
    <?php wp_head(); ?>
    
    <?php echo get_theme_mod('head_code', ''); ?>
 
    <link rel="alternate" type="application/rss+xml" href="<?php bloginfo('rss2_url'); ?>" title="<?php printf( __( '%s latest posts', 'yourtechtherapist' ), esc_html( get_bloginfo('name'), 1 ) ); ?>" />
    <link rel="alternate" type="application/rss+xml" href="<?php bloginfo('comments_rss2_url') ?>" title="<?php printf( __( '%s latest comments', 'yourtechtherapist' ), esc_html( get_bloginfo('name'), 1 ) ); ?>" />
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    
</head>
 
<body <?php body_class(); ?>>
    
<?php echo get_theme_mod('body_code', ''); ?>

<div id="wrapper" class="hfeed">
    
    <!-- CUSTOMIZER VARIABLES - HEADER -->
    <?php include ('template-parts/customizer/header.php'); ?>
    
    <div class="js-warning" aria-hidden="true"><p>This website functions best with JavaScript. Please consider turning JavaScript <strong>ON</strong> in your browser.</p></div>
    
    

    <?php if ( true == get_theme_mod( 'announcement_bar_toggle', false ) ) : ?>
    
        <div id="announcementbar" class="<?php echo $announcementbarpages; ?>"><?php echo get_theme_mod('announcement_bar_content', 'This is your announcement bar content.'); ?></div>
    
    <?php endif; ?>
    
    <div class="header_location <?php echo $header_location; ?>">
    
    <header id="primaryheader" class="<?php echo implode(' ', array($transparent_header, $header_content_layout, $sticky_header)); ?> scrollpassed primary">
        
        <?php if ( true == get_theme_mod('show_secondary_header', false) ) : ?>
        
            <div id="secondaryheader" class="secondary <?php echo $secondaryheaderlayout; ?>">
                
                <div class="wrapper">
                    
                    <div class="text"><?php echo do_shortcode( get_theme_mod( 'secondary_header_text', '' ) ); ?></div>

                    <?php if ( true == get_theme_mod('show_secondary_header_menu', true) ) : ?>

                        <nav class="secondary-menu"><?php wp_nav_menu( array( 'theme_location' => 'secondary-header-menu' ) ); ?></nav>

                    <?php endif; ?>

                    <?php if ( true == get_theme_mod('show_social_menu', false) ) : ?> 

                        <nav class="social-menu"><?php wp_nav_menu( array( 'theme_location' => 'social-menu' ) ); ?></nav>

                    <?php endif; ?>
                
                </div>

            </div>
        
        <?php endif; ?>
        
        <div class="wrapper">
            
            <?php if ( true == get_theme_mod( 'show_site_title', true ) || true == get_theme_mod( 'show_site_tagline', true) || has_custom_logo() ) : ?>
            
                <div class="identity">

                    <?php

                    if ( has_custom_logo() ) {
                        $hasCustomLogo = 'has-custom-logo';
                    }
                    else {
                        $hasCustomLogo = 'no-logo';
                    }

                    if ( function_exists( 'the_custom_logo' ) ) {
                        the_custom_logo();
                    }

                    ?>

                    <?php if ( true == get_theme_mod( 'show_site_title', true ) || true == get_theme_mod( 'show_site_tagline', true)  ) : ?>

                        <div class="<?php echo $hasCustomLogo; ?> site-title">

                            <?php if ( true == get_theme_mod( 'show_site_title', true ) ) : ?>
                                <a class="site-title-link" href="<?php bloginfo('url'); ?>"><h1><?php bloginfo( 'name' );?></h1></a>
                            <?php endif; ?>

                            <?php if ( true == get_theme_mod( 'show_site_tagline', true ) ) : ?>
                                <a class="site-tagline-link" href="<?php bloginfo('url'); ?>"><h2><?php bloginfo( 'description' ); ?></h2></a>
                            <?php endif; ?>

                        </div>

                    <?php endif; ?>

                </div>
                
            <?php endif; ?>
            
            <?php if ( true == get_theme_mod( 'header_show_navigation', true ) || true == get_theme_mod('show_secondary_header_menu', true) || true == get_theme_mod('show_social_menu', false) ) { ?>
                <a href="#nav" class="nav-toggle" aria-label="Open main menu">
                    <span class="sr-only">Open main menu</span>
                    <i class="fas fa-bars" aria-hidden="true"></i>
                </a>

                <nav id="nav" class="no-js main" aria-label="Main menu">

                    <a href="#nav-toggle" class="nav-close" aria-label="Close main menu">
                        <span class="sr-only">Close main menu</span>
                        <span class="fas fa-times" aria-hidden="true"></span>
                    </a>
                    
                    <?php if ( true == get_theme_mod( 'header_show_navigation', true ) ) : ?>
                    
                        <?php wp_nav_menu( array( 'theme_location' => 'header-menu' ) ); ?>
                    
                    <?php endif; ?>
                    
                    <?php if ( true == get_theme_mod( 'header_show_navigation', true ) ) {
                    
                        $shownav = 'shownav';
                    }
                    else {
                        $shownav = 'no_shownav';
                    } ?>
                    
                    <?php if ( true == get_theme_mod('show_secondary_header_menu', false) ) : ?>

                        <?php wp_nav_menu( array( 
                            'theme_location' => 'secondary-header-menu',
                            'container_class'     => 'menu-secondary-header-container ' . $shownav
                    ) ); ?>

                    <?php endif; ?>
                    
                    <?php if ( true == get_theme_mod( 'header_fullscreen_search', false ) ) {
                        $fullscreensearch = 'fullscreen';
                    }
                    else {
                        $fullscreensearch = 'not_fullscreen';
                    } ?>

                    <?php if ( true == get_theme_mod( 'header_show_search', true ) ) { ?>
                    
                        <a class="searchicon <?php echo $fullscreensearch; ?>" href="#searchform"><i class="fas fa-search fa-fw"></i></a>
                    
                    <?php } else {  

                    } ?>

                    <?php get_search_form(); ?>

                    <?php if ( true == get_theme_mod('show_social_menu', false) ) : ?> 

                        <?php wp_nav_menu( array(
                            'theme_location' => 'social-menu',
                            'container_class'     => 'menu-social-menu-container mobile ' . $shownav
                    ) ); ?>

                    <?php endif; ?>

                </nav> <!-- nav -->

                <a href="#main-menu-toggle" class="backdrop" tabindex="-1" aria-hidden="true" hidden></a>
            <?php } else {  

            } ?>
        
        </div>
        
    <?php include('template-parts/addons/sociallinks.php'); ?>
        
    </header><!-- header -->
    
    <!-- CUSTOMIZER VARIABLES - CONTENT -->
    <?php include ('template-parts/customizer/content.php'); ?>
        
    <main class="<?php echo $sticky_header; ?>">