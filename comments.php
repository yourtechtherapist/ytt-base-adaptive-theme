<?php
/**
 * The template file for displaying the comments and comment form for the
 * Your Tech Therapist theme
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0.0
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
*/
if ( post_password_required() ) {
	return;
}

if ( $comments ) {
	?>

	<section class="comments" id="comment-list">
        
        <h3><?php comments_number( 'No comments (0)', 'Comments (1)', 'Comments (%) ' ); ?></h3>

		<?php
		$comments_number = absint( get_comments_number() );
		?>

		<ul class="comments-inner section-inner thin max-percentage comment-list">

			<?php
                // Display comments
                wp_list_comments( array(
                    'callback' => 'better_comments'
                ) );

			$comment_pagination = paginate_comments_links(
				array(
					'echo'      => false,
					'end_size'  => 0,
					'mid_size'  => 0,
					'next_text' => __( 'Newer Comments', 'twentytwenty' ) . ' <span aria-hidden="true">&rarr;</span>',
					'prev_text' => '<span aria-hidden="true">&larr;</span> ' . __( 'Older Comments', 'twentytwenty' ),
				)
			);

			if ( $comment_pagination ) {
				$pagination_classes = '';

				// If we're only showing the "Next" link, add a class indicating so.
				if ( false === strpos( $comment_pagination, 'prev page-numbers' ) ) {
					$pagination_classes = ' only-next';
				}
				?>

				<nav class="comments-pagination pagination<?php echo $pagination_classes; //phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped -- static output ?>" aria-label="<?php esc_attr_e( 'Comments', 'twentytwenty' ); ?>">
					<?php echo wp_kses_post( $comment_pagination ); ?>
				</nav>

				<?php
			} ?>

		</ul><!-- .comments-inner -->

	</section><!-- comments -->

	<?php
}

if ( comments_open() || pings_open() ) {

	comment_form(
		array(
			'class_form'         => 'section-inner thin max-percentage',
			'title_reply_before' => '<h3 id="reply-title" class="comment-reply-title">',
			'title_reply_after'  => '</h3>',
            'class_form'         => 'content-width',
		)
	);

} elseif ( is_single() ) {

	if ( $comments ) {
		echo '<hr class="styled-separator is-style-wide" aria-hidden="true" />';
	}

	?>

        <section class="comment-respond content-width" id="respond">

            <h5 class="comments-closed"><?php _e( 'Comments are closed.', 'yourtechtherapist' ); ?></h5>

        </section><!-- #respond -->

	<?php
}
