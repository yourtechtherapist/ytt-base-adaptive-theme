<?php get_header(); ?>
 
    <div id="container">

        <section>
            
            <?php if (get_field('show_page_title') == true) : ?>
                <h1 class="page-title"><?php the_title(); ?></h1>
            <?php endif; ?>
            
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post();
                the_content();
            endwhile; 
            else: ?>
                <p>Sorry, no posts matched your criteria.</p>
            <?php endif; ?>
        </section>

    </div><!-- #container -->
        
<?php get_footer(); ?>