<?php get_header(); ?>
	
    <div id="container" class="blog index">   
        
        <?php
        
        $post_id = get_option( 'page_for_posts', $post_id );
        
        ?>
        
        <?php if (get_field('show_page_title', $post_id) == true) : ?>
            <h1 class="page-title"><?php echo get_the_title( get_option('page_for_posts', true) ); ?></h1>
        <?php endif; ?>
        
        <?php if ( have_rows( 'banner_image', $post_id ) ) : ?>
            <?php while ( have_rows( 'banner_image', $post_id ) ) : the_row(); ?>
                <?php if ( get_sub_field( 'content_image' ) == 1 ) { ?>
                    <div class="banner">
                        <img src="<?php echo the_sub_field( 'background_image', $post_id ); ?>" alt="banner" />
                <?php } else { ?>
                    <div class="banner" style="background-image: url('<?php echo the_sub_field( 'background_image', $post_id ); ?>'); padding-top: 
                    <?php the_sub_field( 'padding-top', $post_id ); ?>em; padding-bottom: 
                    <?php the_sub_field( 'padding-bottom', $post_id ); ?>em;">
                    <div class="wrapper content-width">
                        <h1 style="color: <?php the_sub_field( 'title_color' ); ?>;"><?php the_sub_field( 'title', $post_id ); ?></h1>
                         <div style="color: <?php the_sub_field( 'content_color' ); ?>;"><?php the_sub_field( 'content', $post_id ); ?></div>
                    </div>
                <?php } ?>
                </div>
            <?php endwhile; ?>
        <?php endif; ?>
        
        <?php if( get_field('top_blurb', $post_id) ): ?>
            <div class="blurb section content-width">
                <?php the_field('top_blurb', $post_id); ?>
            </div>
        <?php endif; ?>
		
        <section class="wrapper content-width">
            
            <?php
            
            $layout = get_theme_mod('blog_layout', 'row');
            
            ?>
            
            <?php
            
            if ( is_active_sidebar( 'blog_widget_area' ) ) {
                $sidebar = 'sidebar_active';
            } else {
                $sidebar = 'sidebar_inactive';
            }
            
            ?>
				
            <div id="content" class="<?php echo $layout; ?> <?php echo $sidebar; ?>">
                
                <!-- Starts the loop -->
                <?php
                    $postsPerPage = get_theme_mod('blog_posts_per_page');
                    $postType = 'post';
                    $args = array(
                            'post_type' => $postType,
                            'posts_per_page' => $postsPerPage,
                    );

                    $blogloop = new WP_Query($args);

                    if ($blogloop->have_posts()) : while ($blogloop->have_posts()) : $blogloop->the_post();

                    $blogExclude[] = get_the_ID();

                    $exclude = implode(',', $blogExclude);

                ?>

                <?php get_template_part( 'template-parts/blog/post' ); ?>

                <!-- Ends the loop -->
                <?php   
                endwhile;
                wp_reset_postdata();

                else : ?>

                    <h4 class="noposts">There are no posts. 
                        <br />Please check back soon!</h4>

                <?php endif; ?>

            </div><!-- #posts -->
                
            <?php if ( is_active_sidebar( 'blog_widget_area' ) ) :
            
                $sidebar = 'sidebar_active';
                    
                ?>
            
                <aside id="sidebar">
                    <?php dynamic_sidebar( 'blog_widget_area' ); ?>
                </aside>
            
            <?php endif; ?>
			
        </section><!-- .wrapper -->

        <?php if (show_posts_nav()) : ?>

            <div class="navigation content-width">
                <div class="older button-arrow left"><?php next_posts_link('Older Posts') ?></div>
                <div class="newer"><?php previous_posts_link('Newer Posts') ?></div>
            </div>

        <?php endif; ?>
			
	</div><!-- #container -->

<?php get_footer(); ?>