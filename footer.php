    <?php if ( true == get_theme_mod( 'back_to_top_button', true ) ) { ?>
        <div class="button scrollup <?php echo get_theme_mod('back_to_top_location', 'right'); ?>" aria-hidden="true"><i class="fas fa-chevron-up"></i></div>
    <?php } else {  

    } ?>    

</main><!-- main -->

</div> <!-- .header_location -->

    <!-- CUSTOMIZER VARIABLES - FOOTER -->
    <?php include ('template-parts/customizer/footer.php'); ?>

    <!-- Three Column Footer - Widgetized -->
    <?php if( is_active_sidebar('footer_one_widget_area') || is_active_sidebar('footer_two_widget_area') || is_active_sidebar('footer_three_widget_area') ) { ?>
        <footer id="primaryfooter" class="<?php echo $footer_content_layout; ?> footer primary widgetized three-column">

            <div class="wrapper"> 
                
                <?php if( is_active_sidebar('footer_one_widget_area') ) { ?>
                    <div class="widgetarea column">

                        <?php dynamic_sidebar('footer_one_widget_area'); ?>

                    </div>
                <?php } ?>
                
                <?php if( is_active_sidebar('footer_two_widget_area') ) { ?>
                    <div class="widgetarea column">

                        <?php dynamic_sidebar('footer_two_widget_area'); ?>

                    </div>
                <?php } ?>

                <?php if( is_active_sidebar('footer_three_widget_area') ) { ?>
                    <div class="widgetarea column">

                        <?php dynamic_sidebar('footer_three_widget_area'); ?>

                    </div>
                <?php } ?>

            </div>

        </footer>
    <?php } ?>
    <!-- End Three Column Footer - Widgetized -->

    <!-- Secondary Footer -->
    <footer id="secondaryfooter" class="secondary">
        
        <div class="wrapper">
                
            <?php if ( true == get_theme_mod( 'menu_in_footer', true ) ) { ?>

                <nav id="nav" class="footer-nav">
                    <?php wp_nav_menu( array( 'theme_location' => 'footer-menu', 'depth' => 1 ) ); ?>
                </nav>

            <?php } else {  

            } ?>   
        
            <div class="copyright">
                <p><?php echo do_shortcode( get_theme_mod( 'footer_copyright', '© Copyright [site-name] [current-year]' ) ); ?></p>
            </div>

            <div class="credit">
                <p>Website created by <a href="https://yourtechtherapist.com">Your Tech Therapist</a></p>
            </div>
            
        </div>
        
    </footer>
    
</div><!-- #wrapper -->

<?php echo get_theme_mod('footer_code', ''); ?>

<?php wp_footer(); ?>

</body>
</html>