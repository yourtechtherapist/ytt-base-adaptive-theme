(function($) {
    
    $(document).ready(function() {
        
        var changeButtons = $('button, input[type="submit"], .button, .wp-block-button .wp-block-button__link, .wp-block-uagb-cf7-styler .uagb-cf7-styler__btn-align-center input.wpcf7-form-control.wpcf7-submit, .wpforms-form button, button.wpforms-submit, .edit-post-visual-editor.editor-styles-wrapper .block-editor-block-list__layout  .wp-block .wp-block-button .wp-block-button__link, .edit-post-visual-editor.editor-styles-wrapper .block-editor-block-list__layout  .wp-block .button');
        var searchButton = $('.searchform.fullscreen button#searchsubmit');
        var scrollUp = $('.scrollup');

        console.log(changeButtons);

        var colorOrgArray = []; 
        var backgroundOrgArray = [];
        var borderOrgArray = [];

         $.each(changeButtons, function (index, button) {
             console.log(button + ' loop ' + index);

             colorOrgArray.push($(this).css('color'));
           backgroundOrgArray.push($(this).css('background-color'));
           borderOrgArray.push($(this).css('border-color'));
         });

          console.log(backgroundOrgArray);

        $.each(changeButtons, function (index, button) {
           button.addEventListener('mouseenter', function() {
             ManageButtonHover(index);
           });
         button.addEventListener('mouseleave', function() {
             ManageButtonHoverOut(index);
           });
        });


        function ManageButtonHover (_index) {
            console.log('Entered ManageButtonHover: ' + _index);

          var color =$(changeButtons[_index]).css('color');
          var background = $(changeButtons[_index]).css('background-color');
          var border = $(changeButtons[_index]).css('border-color');

          $(changeButtons[_index]).css({
            'color' : background,
            'background-color' : color,
            'border-color' : background
          });
        }

        function ManageButtonHoverOut(_index) {
        console.log('Entered ManageButtonHoverOut: ' + _index);
        console.log(backgroundOrgArray[_index]);


         $(changeButtons[_index]).css({
           'color' : colorOrgArray[_index],
           'background-color' : backgroundOrgArray[_index],
           'border-color' : borderOrgArray[_index]
         });
        }
        
    });
    
})(jQuery);