(function($) {
    
    function padMain() {
        
        // Add padding to <main> when Sticky Header option is selected in the Customizer
        var header = $('header.primary');
        var headerLocation = $('.header_location');
        var headerHeight = header[0].getBoundingClientRect().height;
        var headerWidth = header[0].getBoundingClientRect().width;
        var main = $('main');
        var footer = $('footer');
        
        if(header.hasClass('sticky_header') && header.hasClass('not_transparent') && headerLocation.hasClass('location_top')) {
            main.css('padding-top' , headerHeight + 'px');
        } else if(header.hasClass('sticky_header') && header.hasClass('not_transparent') && headerLocation.hasClass('location_top')) {
            main.css('padding-bottom' , headerHeight + 'px');
        } else if(header.hasClass('sticky_header') && headerLocation.hasClass('location_left')) {
            main.css('padding-left' , headerWidth + 'px');
            footer.css('padding-left' , headerWidth + 'px');
        } else if(header.hasClass('sticky_header') && headerLocation.hasClass('location_right')) {
            main.css('padding-right' , headerWidth + 'px');
            footer.css('padding-right' , headerWidth + 'px');
        }
        
    }
    
    $(document).ready(function() {
        
        padMain();
        
    });
    
    $(window).resize(function() {
        
        padMain();
        
    });
    
    // Stop scroll up button at footer
    $(document.body).on('touchmove', stickyHeader); // for mobile
    $(window).on('scroll', stickyHeader); 
    
    function stickyHeader() {

        var header = $('header.primary');
        var scrollPassed = $(window).height();
        
        if($(scrollPassed).length && $(window).scrollTop() > scrollPassed / 4) {
           $(header).addClass('scroll');
        } else {
            $(header).removeClass('scroll');
        }
        
    }
    
})(jQuery);