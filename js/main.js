(function($) {
    
    $(document).ready(function() {
        
        var scrollup = $('.scrollup');
        var root = $('html, body');
             
        $(scrollup).click(function() {
            $(root).animate({ scrollTop: 0 }, 500);
            
            return false;
        });
        
        // VH fix for mobile browsers
        let vh = window.innerHeight * 0.01;
        document.documentElement.style.setProperty('--vh', `${vh}px`);
        
        anchorScroll();
        
    });
    
    // Stop scroll up button at footer
    $(document.body).on('touchmove', onScroll); // for mobile
    $(window).on('scroll', onScroll); 
    
    function onScroll() {

        var scrollup = $('.scrollup');
        var scrollPassed = $(window).height();
        var footer = $('footer');
        var footerHeight = $('footer').outerHeight;
        
        if($(scrollPassed).length && $(window).scrollTop() > scrollPassed / 2) {
           $(scrollup).fadeIn();
        } else {
            $(scrollup).fadeOut();
        }
        
        if ($(scrollup).offset().top + $(scrollup).height() >= $(footer).offset().top - 16) {
            $(scrollup).css({
                'position': 'absolute'
            });
        } else if ($(document).scrollTop() + window.innerHeight < $(footer).offset().top) {
            $(scrollup).css({
                'position': 'fixed'
            });
        }
        
        // Adjust scroll up button position based on outdated browser notice
        var outdated = $('#outdated');
        var outdatedHeight = $('#outdated').outerHeight();
        
        if(outdated.is(':visible')){
            
            if(scrollup.css('position') == 'fixed') {
                scrollup.css('bottom' , outdatedHeight + 16);
            } else if ((scrollup.css('position') == 'absolute')) {
                scrollup.css('bottom' , outdatedHeight + 16);
            }
        }
        
    }
    
    // Anchor scrolling
    function anchorScroll() {
        
        var root = $('html, body');
        var anchor = $("main a[href^='#'], footer a[href^='#']").not('main .wp-block-getwid-tabs a.ui-tabs-anchor');
        var headerHeight = $('header.primary').outerHeight();
        
        anchor.click(function() {
            var href = $.attr(this, 'href');
            root.animate({
                scrollTop: $(href).offset().top - headerHeight
            }, 500, function () {
                window.location.hash = href;
            });

            return false;
        });
        
    }
    
    // Select2
    $(document).ready(function() {
        $('main select:not(.wpforms-container select)').select2({
            width: '100%'
        });
    });
    
})(jQuery);