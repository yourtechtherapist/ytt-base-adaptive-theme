(function($) {
    
    $(document).ready(function() {
        
        function mobileMenu() {
            
            // Mobile Menu
            var nav = $('header nav#nav');
            var navToggle = $('header.primary .nav-toggle');
            var navClose = $('header.primary nav#nav .nav-close, header.primary .backdrop');
            var backdrop = $('header.primary .backdrop');

            navToggle.click(function(){
                event.preventDefault();
                nav.fadeIn(250).addClass('backdrop-open');
                backdrop.fadeIn(250);
            });

            navClose.click(function(){
                event.preventDefault();
                nav.fadeOut(250);
                setTimeout(function(){
                    nav.removeClass('backdrop-open');
                }, 250);
                backdrop.fadeOut(250);
            });
            
        }
        
        mobileMenu();
        
        function dropdownMenu() {
            
            // Dropdown navigation
            var parent = $('header nav:not(".open") .page_item_has_children, header nav:not(".open") .menu-item-has-children');

            function dropDown() {
                $(this).children('ul').slideDown(250);
            }
            
            function dropUp() {
                $(this).children('ul').slideUp(250);
            }
            
            parent.hoverIntent({
                over: dropDown,
                out: dropUp,
                timeout: 250,
            });
            
        }
        
        if( $('header.primary .nav-toggle').is(':hidden') ) {
            dropdownMenu();
        }
        
        function searchButton () {
            
            // Search icon
            var searchButton = $('header .searchicon');
            var searchClose = $('header .searchform .close');
            var searchForm = $('header .searchform');

            searchButton.click(function() {
                event.preventDefault();
                searchForm.slideToggle(250).css('display' , 'flex');
                searchButton.toggleClass('open');
            });
            
            searchClose.click(function() {
                searchForm.slideToggle(250);
                searchButton.removeClass('open');
            });
            
        }
        
        searchButton();
        
    });
    
})(jQuery);