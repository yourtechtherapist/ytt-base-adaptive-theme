jQuery(function($){ // use jQuery code inside this to avoid "$ is not defined" error
    
    $(document).ready(function(){
        
        var button = $('#more_posts');
        var ppp = $(button).data('ppp'); // Post per page
        var exclude = $(button).data('exclude');
        var postType = $(button).data('post_type');
        var pageNumber = 1;


        function load_posts(){
            pageNumber++;
            $.ajax({
                type: "POST",
                dataType: "html",
                url: ajax_posts.ajaxurl,
                data: {
                    pageNumber: pageNumber,
                    ppp: ppp,
                    postType: postType,
                    exclude: exclude,
                    action: 'events_ajax'
                },
                beforeSend : function ( xhr ) {
                    button.text('Loading...');
                },
                success: function(data){
                    var $data = $(data);
                    if($data.length){
                        $(".ajax-posts").append($data);
                        $(".ajax-posts .loadmore").animate({opacity: '1'}, 500);
                        $(button).attr("disabled",false);
                        $(button).text('Load More'); // preloader here
                    } else{
                        $(button).attr("disabled",true);
                        $(button).replaceWith('<h4>No more posts...</h4>');
                    }
                },
                error : function(jqXHR, textStatus, errorThrown) {
                    $loader.html(jqXHR + " :: " + textStatus + " :: " + errorThrown);
                }

            });
            return false;
        }

        $(button).on("click",function(){ // When btn is pressed.
            $(button).attr("disabled",true); // Disable the button, temp.
            load_posts();
        });
        
    });
    
});