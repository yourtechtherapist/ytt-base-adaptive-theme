// Unregister default Gutenberg block styles
wp.domReady( () => {
	wp.blocks.unregisterBlockStyle(
		'core/button',
		[ 'default', 'outline', 'squared', 'fill' ]
	);
} );


// Register Gutenberg block styles
wp.domReady( () => {

	wp.blocks.registerBlockStyle( 'core/heading', [ 
		{
			name: 'default',
			label: 'Default',
			isDefault: true,
		},
		{
			name: 'alt',
			label: 'Alt Heading',
		}
	]);
    
    wp.blocks.registerBlockStyle( 'core/paragraph', [ 
		{
			name: 'default',
			label: 'Default',
			isDefault: true,
		},
		{
			name: 'disclaimer',
			label: 'Disclaimer Text',
		}
	]);
    
    wp.blocks.registerBlockStyle( 'core/button', [ 
		{
			name: 'default',
			label: 'Default',
			isDefault: true,
        },
		{
			name: 'nopadding',
			label: 'No Padding',
		}
	]);
    
    wp.blocks.registerBlockStyle( 'core/columns', [
        {
            name: 'default',
            label: 'Default',
            isDefault: true,
        },
        {
            name: 'nospacing',
            label: 'No Spacing',
        }
    ]);
    
    wp.blocks.registerBlockStyle( 'core/image', [ 
		{
			name: 'default',
			label: 'Default',
			isDefault: true,
		},
		{
			name: 'bordered',
			label: 'Bordered',
		}
	]);
    
    wp.blocks.registerBlockStyle( 'core/gallery', [ 
		{
			name: 'default',
			label: 'Default',
			isDefault: true,
		},
		{
			name: 'vcentered',
			label: 'Centered Vertically',
		}
	]);
    
} );