<?php get_header(); ?>
        
    <div id="container" class="post single">    

        <section id="content" class="post-content">

            <?php if (has_post_thumbnail()) {
                $thumbnail = 'thumbnail';
            } else {
                $thumbnail = 'no_thumbnail';
            } ?>

            <div class="featured-image <?php echo $thumbnail; ?>">

                <?php the_post_thumbnail( 'wide_featured' ); ?>

            </div>

            <?php the_post(); ?>

                <article id="post-<?php the_ID(); ?>" <?php post_class('post'); ?>>

                    <h1 class="entry-title is-style-alt"><?php the_title(); ?></h1>

                    <div class="entry-meta accent-color-text">
                        
                        <span class="vcard">
                            <?php echo get_avatar( get_the_author_meta( 'ID' ), 50 ); ?>
                        </span>

                        <span class="author name"><?php the_author(); ?></span>

                        <span class="author meta-sep"> | </span>

                        <span class="author date"><abbr class="published" title="<?php the_time('Y-m-d') ?>"><?php the_time( get_option( 'date_format' ) ); ?></abbr></span>

                    </div><!-- .entry-meta -->

                    <div class="entry-content">
                        <?php the_content(); ?>
                    </div><!-- .entry-content -->

                    <div class="entry-utility accent-color-text">
                        <?php if (has_category()) : ?>
                            <span class="cat-links"><?php echo get_the_category_list(', '); ?></span>
                        <?php endif; ?>
                        <?php if (has_tag()) : ?>
                            <span class="tag-links"><?php echo get_the_tag_list('',', ',''); ?></span>
                        <?php endif; ?>
                    </div><!-- .entry-utility -->                                                                                                   
                </article><!-- #post-<?php the_ID(); ?> -->                   

            </section><!-- #content -->                                          

        <?php comments_template('', true); ?>

    </div><!-- #container -->
				
<?php get_footer(); ?>